package com.cubuanic.stepik.java.interview;


import com.cubuanic.stepik.java.interview.step4_3_02.BotRequestMapping;
import com.cubuanic.stepik.java.interview.step4_3_02.Operation;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Step4_3_02 {
    public static Map<String, Operation> createPoolOperation(List<Object> objects) throws Exception {
        Map<String, Operation> operations = new HashMap<>();

        for (Object object : objects) {
            for (Method m : object.getClass().getDeclaredMethods()) {
                if (!m.isSynthetic() && m.isAnnotationPresent(BotRequestMapping.class)) {
                    m.setAccessible(true);
                    operations.put(
                        m.getDeclaredAnnotation(BotRequestMapping.class).value(),
                        () -> m.invoke(object)
                    );
                }
            }
        }

        return operations;
    }
}
