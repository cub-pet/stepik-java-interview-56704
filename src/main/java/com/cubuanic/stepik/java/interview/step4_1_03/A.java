package com.cubuanic.stepik.java.interview.step4_1_03;

import com.cubuanic.Generated;

// avoid by jacoco
@Generated(message = "manual")
public class A {
    final int age;
    final String name;

    public A(int age, String name) {
        this.age = age;
        this.name = name;
    }
}
