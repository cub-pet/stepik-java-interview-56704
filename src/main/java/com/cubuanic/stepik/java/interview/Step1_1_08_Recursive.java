package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step1_1_08.Node;

public class Step1_1_08_Recursive {
    public static Node reverse(Node head) {
        if (head == null || head.getNext() == null) {
            return head;
        }

        final Node tail = reverse(head.getNext());
        head.setNext(null);

        Node end = tail;
        while (end.getNext() != null) {
            end = end.getNext();
        }
        end.setNext(head);

        return tail;
    }
}
