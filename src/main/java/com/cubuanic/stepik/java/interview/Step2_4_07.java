package com.cubuanic.stepik.java.interview;

import com.cubuanic.Generated;

import java.util.List;

public class Step2_4_07 {
    // avoid by jacoco
    @Generated(message = "manual")
    public static void genConcurrentModificationException(List<Integer> integers) {
        Integer sum = 0;
        for (Integer item : integers) {
            if (item % 3 == 0) {
                integers.remove(item);
            }
            sum += item;
        }
    }
}

