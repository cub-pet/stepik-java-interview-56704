package com.cubuanic.stepik.java.interview;

import java.util.concurrent.atomic.AtomicBoolean;

public class Step3_4_04 {
    volatile int count = 0;

    private AtomicBoolean atomicBoolean = new AtomicBoolean(false);

    void safeIncrement() {
        while (true) {
            if (atomicBoolean.compareAndSet(false, true)) {
                break;
            }
        }
        count++;
        atomicBoolean.set(false);
    }

    public int getCount() {
        return count;
    }
}
