package com.cubuanic.stepik.java.interview;


import com.cubuanic.stepik.java.interview.step4_3_01.Autowired;
import com.cubuanic.stepik.java.interview.step4_3_01.CandidateNotFindException;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Step4_3_01 {

    public static void dependencyInjection(List<Object> objects) throws Exception {
        Map<? extends Class<?>, Object> objByClass = objects
            .stream()
            .collect(Collectors.toMap(
                Object::getClass,
                Function.identity()
            ));

        for (Object object : objects) {
            for (Field f : object.getClass().getDeclaredFields()) {
                if (!f.isSynthetic() && f.isAnnotationPresent(Autowired.class)) {
                    final Class<?> wiredTo = f.getType();
                    if (!objByClass.containsKey(wiredTo)) {
                        throw new CandidateNotFindException();
                    }
                    f.setAccessible(true);
                    f.set(object, objByClass.get(wiredTo));
                }
            }
        }
    }
}
