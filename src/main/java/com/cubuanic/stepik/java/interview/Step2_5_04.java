package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step2_5_04.Box;

public class Step2_5_04 {
    public static Box<Integer> boxingValue(double value) {
        Box<Double> d = new Box<>(value);
        Box<?> x = d;
        return (Box<Integer>) x;
    }
}
