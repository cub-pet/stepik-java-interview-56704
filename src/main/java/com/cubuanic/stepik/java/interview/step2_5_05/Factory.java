package com.cubuanic.stepik.java.interview.step2_5_05;

import java.lang.reflect.ParameterizedType;

public abstract class Factory<T> {
    public Factory() {

    }

    public T generateInstance() throws Exception {
        Class factoryClass = this.getClass();
        final ParameterizedType type = (ParameterizedType) factoryClass.getGenericSuperclass();
        Class parameter = (Class) type.getActualTypeArguments()[0];
        return (T) parameter.getDeclaredConstructor().newInstance();
    }
}
