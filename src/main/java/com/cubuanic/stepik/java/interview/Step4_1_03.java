package com.cubuanic.stepik.java.interview;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Step4_1_03 {
    public static <T> void printAllClassFields(Class<T> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        Arrays.stream(fields)
            .filter(f -> !f.isSynthetic())
            .map(Field::getName)
            .sorted()
            .forEach(System.out::println);
    }
}
