package com.cubuanic.stepik.java.interview;

import java.util.Scanner;

public class Step1_1_04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        checkPalindrom(scanner.nextLine());
    }

    private static void checkPalindrom(String s) {
        final String normalized = s.replaceAll("[ '’\\p{Punct}]+", "").toLowerCase();
        if (normalized.equals(new StringBuilder(normalized).reverse().toString())) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }
}
