package com.cubuanic.stepik.java.interview.step4_3_02;

import com.cubuanic.Generated;

public class A {
    @BotRequestMapping("hello")
    void methodA1() {
        System.out.println("hello world!!!");
    }

    @BotRequestMapping("goodbye")
    void methodA2() {
        System.out.println("goodbye my friend!!!");
    }

    // avoid by jacoco
    @Generated(message = "manual")
    void methodA3() {
        System.out.println("bad call!!");
    }
}
