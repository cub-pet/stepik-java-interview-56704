package com.cubuanic.stepik.java.interview;

import com.cubuanic.Generated;

import java.util.ArrayList;
import java.util.List;

public class Step2_4_06 {
    // avoid by jacoco
    @Generated(message = "manual")
    public static List<Integer> generateBadList() {
        List list = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            list.add(Integer.valueOf(i));
        }
        list.add(new Object());
        return list;
    }
}

