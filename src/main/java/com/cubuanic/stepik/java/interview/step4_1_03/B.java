package com.cubuanic.stepik.java.interview.step4_1_03;

import com.cubuanic.Generated;

import java.util.Comparator;

// avoid by jacoco
@Generated(message = "manual")
public class B {
    final Comparator cmp;
    final A a;

    public B(Comparator cmp, A a) {
        this.cmp = cmp;
        this.a = a;
    }
}
