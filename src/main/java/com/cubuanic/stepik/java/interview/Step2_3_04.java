package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step2_3_04.Complex;

public class Step2_3_04 {
    public static Complex createComplex(double real, double image) {
        class ComplexImpl implements Complex {
            double real;
            double image;

            public ComplexImpl(double real, double image) {
                this.real = real;
                this.image = image;
            }

            @Override
            public Complex sum(Complex other) {
                return new ComplexImpl(
                    this.real + other.getReal(),
                    this.image + other.getImage()
                );
            }

            @Override
            public Complex sub(Complex other) {
                return new ComplexImpl(
                    this.real - other.getReal(),
                    this.image - other.getImage()
                );
            }

            @Override
            public Complex mul(Complex other) {
                return new ComplexImpl(
                    this.real * other.getReal() - this.image * other.getImage(),
                    this.real * other.getImage() + this.image * other.getReal()
                );
            }

            @Override
            public double getReal() {
                return real;
            }

            @Override
            public double getImage() {
                return image;
            }

            @Override
            public int hashCode() {
                return this.toString().hashCode();
            }

            @Override
            public boolean equals(Object obj) {
                if (this == obj) return true;
                if (obj instanceof Complex) {
                    return this.toString().equals(obj.toString());
                } else {
                    return false;
                }
            }

            @Override
            public String toString() {
                final String roundedR = round(real);
                final String roundedI = round(image);
                if (Double.parseDouble(roundedI) < 0) {
                    return roundedR + "-" + roundedI + "i";
                } else if (Double.parseDouble(roundedI) == 0) {
                    return roundedR;
                } else {
                    return roundedR + "+" + roundedI + "i";
                }
            }

            private String round(double value) {
                return String.valueOf(Math.round(value * 100.0) / 100.0);
            }
        }

        return new ComplexImpl(real, image);
    }
}

