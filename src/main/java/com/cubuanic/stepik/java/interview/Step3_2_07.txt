Map<String, Double> actual = workers.stream()
    .collect(
        Collectors.groupingBy(
            Worker::getPosition,
            Collectors.averagingDouble(Worker::getSalary)
        )
    );
    