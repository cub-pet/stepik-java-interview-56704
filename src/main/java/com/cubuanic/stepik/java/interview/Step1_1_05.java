package com.cubuanic.stepik.java.interview;

import java.util.Scanner;

public class Step1_1_05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        printFibonacci(scanner.nextLong());
    }

    private static void printFibonacci(long n) {
        if (n == 0 || n == 1) {
            System.out.println(n);
            return;
        }

        long fib1 = 0;
        long fib2 = 1;
        long result;
        do {
            result = fib1 + fib2;
            fib1 = fib2;
            fib2 = result;
        } while (n-- > 2);
        System.out.println(result);
    }
}
