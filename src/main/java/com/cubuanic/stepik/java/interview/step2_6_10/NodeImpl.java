package com.cubuanic.stepik.java.interview.step2_6_10;

public class NodeImpl implements Node {
    String name;
    Node left;
    Node right;
    Node parent;

    public NodeImpl(String name) {
        this.name = name;
        this.left = null;
        this.right = null;
    }

    public NodeImpl(String name, Node left) {
        this.name = name;
        this.left = left;
        left.setParent(this);
        this.right = null;
    }

    public NodeImpl(String name, Node left, Node right) {
        this.name = name;
        this.left = left;
        left.setParent(this);
        this.right = right;
        right.setParent(this);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Node getLeft() {
        return left;
    }

    @Override
    public Node getRight() {
        return right;
    }

    @Override
    public Node getParent() {
        return parent;
    }

    @Override
    public void setParent(Node parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        String parentName = getParent() != null ? getParent().getName() : "?";
        String leftName = getLeft() != null ? getLeft().getName() : "?";
        String rightName = getRight() != null ? getRight().getName() : "?";
        return getName() + "(" + parentName + "," + leftName + "," + rightName + ")";
    }
}