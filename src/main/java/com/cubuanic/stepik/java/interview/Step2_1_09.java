package com.cubuanic.stepik.java.interview;

public class Step2_1_09 {
    public static int reverseByte(int n) {
        int result = 0;

        for (int i = 0; i < 4; i++) {
            result = (result << 8) | (n & 0xff);
            n >>>= 8;
        }

        return result;
    }
}
