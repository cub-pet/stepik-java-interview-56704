package com.cubuanic.stepik.java.interview.step2_5_05;

public class A {
    Integer a;

    public A() {
        this.a = 1;
    }

    public Integer getA() {
        return a;
    }
}
