package com.cubuanic.stepik.java.interview.step1_1_08;


public interface Node {
    Node getNext();

    void setNext(Node next);

    String getPayload();
}
