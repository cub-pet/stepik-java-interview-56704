package com.cubuanic.stepik.java.interview;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class Step3_1_05 {
    public static LocalDateTime toMoscowTime(LocalDateTime localDateTime, String zoneName) {
        return localDateTime
                .atZone(ZoneId.of(zoneName))
                .withZoneSameInstant(ZoneId.of("Europe/Moscow"))
                .toLocalDateTime();
    }
}
