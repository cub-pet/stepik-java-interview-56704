package com.cubuanic.stepik.java.interview;

import java.util.concurrent.locks.ReentrantLock;

public class Step3_4_03 {
    volatile int count = 0;

    ReentrantLock lock = new ReentrantLock();

    void safeIncrement() {
        lock.lock();
        count++;
        lock.unlock();
    }

    public int getCount() {
        return count;
    }
}
