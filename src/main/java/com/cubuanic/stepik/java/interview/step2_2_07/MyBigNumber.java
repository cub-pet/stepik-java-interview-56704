package com.cubuanic.stepik.java.interview.step2_2_07;

public class MyBigNumber implements BigNumber {
    public static final int SERIES_LEN = 18;
    public static final long SERIES_MAX_VAL = 1_000_000_000_000_000_000L;
    private static final byte ADD = 0;
    private static final byte SUB = 1;
    private final String number;
    private final int sign;
    private final int length;

    public MyBigNumber(BigNumber that) {
        this(that.toString());
    }

    public MyBigNumber(String number) {
        final String normalized = number.replaceFirst("^0+(?!$)", "").replaceAll("_", "");
        this.number = normalized;
        this.length = normalized.length();
        this.sign = normalized.charAt(0) == '-' ? -1
                : length == 1 && Integer.parseInt(number) == 0 ? 0
                : 1;
    }

    private MyBigNumber negate(MyBigNumber that) {
        final String thatStr = that.toString();
        if (that.sign == 1) {
            return new MyBigNumber("-" + thatStr);
        } else if (that.sign == -1) {
            return new MyBigNumber(thatStr.substring(1));
        }
        return new MyBigNumber(thatStr);
    }

    private MyBigNumber addSub(MyBigNumber a, MyBigNumber b, byte op) {
        if (op == SUB && a.compareTo(b) == -1) {
            return negate(new MyBigNumber(b.sub(a)));
        }

        final String[] asplit = splitToSeries(a.number);
        final String[] bsplit = splitToSeries(b.number);
        final int limit = Math.max(asplit.length, bsplit.length);
        final StringBuilder result = new StringBuilder(Math.max(a.length, b.length) + 1);

        long carry = 0;
        for (int i = 0; i < limit; i++) {
            long an = i < asplit.length ? Long.parseLong(asplit[i].trim()) : 0;
            long bn = i < bsplit.length ? Long.parseLong(bsplit[i].trim()) : 0;
            carry = op == ADD
                    ? addWithCarry(result, carry, an, bn)
                    : subWithCarry(result, carry, an, bn);
        }

        if (op == ADD && carry != 0) {
            result.insert(0, 1);
        }

        return new MyBigNumber(result.toString());
    }

    private long addWithCarry(StringBuilder result, long carry, long an, long bn) {
        long rn = an + bn + carry;
        if (rn >= SERIES_MAX_VAL) {
            rn -= SERIES_MAX_VAL;
            carry = 1;
        } else {
            carry = 0;
        }
        result.insert(0, toZeroAlignedString(String.valueOf(rn)));
        return carry;
    }

    private long subWithCarry(StringBuilder result, long carry, long an, long bn) {
        long rn = an - bn - carry;
        if (rn < 0) {
            rn += SERIES_MAX_VAL;
            carry = 1;
        } else {
            carry = 0;
        }
        result.insert(0, toZeroAlignedString(String.valueOf(rn)));
        return carry;
    }

    private String spaces(int i) {
        return alignmentStr(i, " ");
    }

    private String zeros(int i) {
        return alignmentStr(i, "0");
    }

    private String alignmentStr(int alignmentLength, String s) {
        return new String(new char[alignmentLength]).replace("\0", s);
    }

    private String toSpaceAlignedString(String str) {
        final String alignment = str.length() % SERIES_LEN == 0
                ? ""
                : spaces(SERIES_LEN - str.length() % SERIES_LEN);
        return alignment + str;
    }

    private String toZeroAlignedString(String str) {
        final String alignment = str.length() % SERIES_LEN == 0
                ? ""
                : zeros(SERIES_LEN - str.length() % SERIES_LEN);
        return alignment + str;
    }

    private String[] splitToSeries(String str) {
        return reverseArray(toSpaceAlignedString(str).split("(?<=\\G.{" + SERIES_LEN + "})"));
    }

    private String[] reverseArray(String[] array) {
        final int length = array.length;
        final String[] temp = new String[length];

        for (int i = 0; i < length; i++) {
            temp[length - i - 1] = array[i];
        }
        return temp;
    }

    @Override
    public BigNumber add(BigNumber bigNumber) {
        MyBigNumber that = new MyBigNumber(bigNumber);
        if (that.sign == 0) {
            return new MyBigNumber(this);
        }
        if (sign == 0) {
            return new MyBigNumber(that);
        }

        if (sign == 1) {
            if (that.sign == 1) {
                return addSub(this, that, ADD);
            } else {
                return addSub(this, negate(that), SUB);
            }
        } else {
            if (that.sign == 1) {
                return addSub(that, negate(this), SUB);
            } else {
                return negate(addSub(negate(this), negate(that), ADD));
            }
        }
    }

    @Override
    public BigNumber sub(BigNumber bigNumber) {
        return add(negate(new MyBigNumber(bigNumber)));
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) throw new NullPointerException();
        if (!(o instanceof BigNumber)) throw new ArithmeticException();

        BigNumber bn = (BigNumber) o;
        MyBigNumber that = new MyBigNumber(bn);

        if (sign < that.sign) {
            return -1;
        } else if (sign > that.sign) {
            return 1;
        } else if (length > that.length) {
            return sign;
        } else if (length < that.length) {
            return -sign;
        } else if (number.equals(that.number)) {
            return 0;
        } else {
            return number.compareTo(that.number);
        }
    }

    @Override
    public String toString() {
        return number;
    }
}