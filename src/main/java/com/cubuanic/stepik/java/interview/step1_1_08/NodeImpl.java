package com.cubuanic.stepik.java.interview.step1_1_08;

public class NodeImpl implements Node {
    String payload;
    Node next;

    public NodeImpl() {
    }

    public NodeImpl(String payload) {
        this.payload = payload;
    }

    @Override
    public Node getNext() {
        return next;
    }

    @Override
    public void setNext(Node next) {
        this.next = next;
    }

    @Override
    public String getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        if (getNext() != null) {
            return getPayload() + "," + getNext().toString();
        } else {
            return getPayload();
        }
    }
}