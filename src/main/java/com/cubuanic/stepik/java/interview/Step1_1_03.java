package com.cubuanic.stepik.java.interview;

import java.util.Scanner;

public class Step1_1_03 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        fizzBuzzPrint(scanner.nextInt());
    }

    private static void fizzBuzzPrint(int i) {
        if (i % 15 == 0) {
            System.out.println("FizzBuzz");
            return;
        }
        if (i % 3 == 0) {
            System.out.println("Fizz");
            return;
        }
        if (i % 5 == 0) {
            System.out.println("Buzz");
            return;
        }
        System.out.println(i);
    }
}
