package com.cubuanic.stepik.java.interview.step2_6_09;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class TreeIterator implements Iterator<Node> {
    private List<Node> nodes = new LinkedList<>();
    private Iterator<Node> iterator;

    public TreeIterator(Node root) {
        addNode(root);
        this.iterator = nodes.iterator();
    }

    private void addNode(Node node) {
        if (node == null) {
            return;
        }
        nodes.add(node);
        addNode(node.getLeft());
        addNode(node.getRight());
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public Node next() {
        return iterator.next();
    }
}
