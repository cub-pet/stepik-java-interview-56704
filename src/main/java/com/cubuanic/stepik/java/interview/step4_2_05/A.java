package com.cubuanic.stepik.java.interview.step4_2_05;

import com.cubuanic.Generated;

// avoid by jacoco
@Generated(message = "manual")
public class A {
    @Weight(10)
    void method1() {
        System.out.println("method1");
    }

    @Weight(3)
    void method2() {
        System.out.println("method2");
    }

    @Weight(21)
    void method3() {
        System.out.println("method3");
    }
}
