package com.cubuanic.stepik.java.interview;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Step3_1_04 {
    public static DateTimeFormatter format() {
        return DateTimeFormatter.ofPattern("yyyy-'M'M-'W'W-E:HH:mm", Locale.UK);
    }
}
