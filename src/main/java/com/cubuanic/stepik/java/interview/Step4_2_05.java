package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step4_2_05.Weight;

import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Step4_2_05 {

    public static void callMethodByWeight(Object object) throws Exception {
        final List<Method> methods = Arrays
            .stream(object.getClass().getDeclaredMethods())
            .filter(m -> m.isAnnotationPresent(Weight.class))
            .map(m -> new AbstractMap.SimpleEntry<>(m, m.getDeclaredAnnotation(Weight.class).value()))
            .sorted(Map.Entry.comparingByValue())
            .map(AbstractMap.SimpleEntry::getKey)
            .collect(Collectors.toList());

        for (Method m : methods) {
            m.setAccessible(true);
            m.invoke(object);
        }
    }
}
