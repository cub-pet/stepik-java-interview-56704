package com.cubuanic.stepik.java.interview.step4_3_02;

import com.cubuanic.Generated;

public class B {
    @BotRequestMapping("palindrome")
    void methodB1() {
        System.out.println("madam I'm adam");
    }

    @BotRequestMapping("cat")
    void methodB2() {
        System.out.println("A cat may look at a king");
    }

    // avoid by jacoco
    @Generated(message = "manual")
    void methodB3() {
        System.out.println("bad call B3!!");
    }

    @BotRequestMapping("dog")
    void methodB4() {
        System.out.println("A barking dog never bites");
    }
}
