package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step3_1_07.WorkingDayTime;

import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Step3_1_07 {
    public static String getOperationTime(Map<DayOfWeek, WorkingDayTime> dayTimeMap) {
        List<AbstractMap.SimpleEntry<
            WorkingDayTime,
            AbstractMap.SimpleEntry<DayOfWeek, DayOfWeek>
            >> opTime = new ArrayList<>();

        WorkingDayTime wdt = null;
        for (DayOfWeek dow : DayOfWeek.values()) {
            if (dayTimeMap.containsKey(dow)) {
                final WorkingDayTime dtmValue = dayTimeMap.get(dow);
                if (wdt == null
                    || !wdt.getStart().equals(dtmValue.getStart())
                    || !wdt.getEnd().equals(dtmValue.getEnd())
                ) {
                    wdt = dtmValue;
                    addNewEntry(opTime, wdt, dow);
                } else {
                    finishLastEntry(opTime, dow);
                }
            } else {
                wdt = null;
                if (lastEntryIsWorkday(opTime)) {
                    addNewEntry(opTime, null, dow);
                } else {
                    finishLastEntry(opTime, dow);
                }
            }
        }

        return formatOpTime(opTime);
    }

    private static String formatOpTime(
        List<AbstractMap.SimpleEntry<
            WorkingDayTime,
            AbstractMap.SimpleEntry<DayOfWeek, DayOfWeek>
            >> opTime
    ) {
        List<String> result = new ArrayList<>();

        for (AbstractMap.SimpleEntry<
            WorkingDayTime,
            AbstractMap.SimpleEntry<DayOfWeek, DayOfWeek>
            > item : opTime
        ) {
            WorkingDayTime wdt = item.getKey();
            DayOfWeek start = item.getValue().getKey();
            DayOfWeek end = item.getValue().getValue();

            StringBuilder sb = new StringBuilder();

            sb.append(
                DateTimeFormatter
                    .ofPattern("EE", Locale.forLanguageTag("ru"))
                    .format(start)
            );

            if (end != null) {
                final String endStr = DateTimeFormatter
                    .ofPattern("EE", Locale.forLanguageTag("ru"))
                    .format(end);

                if (end.getValue() - start.getValue() > 1) {
                    sb.append("-").append(endStr);
                } else {
                    sb.append(", ").append(endStr);
                }
            }

            if (wdt == null) {
                sb.append(" выходной");
            } else {
                sb.append(" ")
                    .append(wdt.getStart().toString())
                    .append("-")
                    .append(wdt.getEnd().toString());
            }

            result.add(sb.toString());
        }

        return String.join(", ", result);
    }

    private static void addNewEntry(
        List<AbstractMap.SimpleEntry<
            WorkingDayTime,
            AbstractMap.SimpleEntry<DayOfWeek, DayOfWeek>
            >> opTime,
        WorkingDayTime wdt,
        DayOfWeek start
    ) {
        opTime.add(
            new AbstractMap.SimpleEntry<>(
                wdt,
                new AbstractMap.SimpleEntry<>(
                    start, null
                )
            )
        );
    }

    private static void finishLastEntry(
        List<AbstractMap.SimpleEntry<
            WorkingDayTime,
            AbstractMap.SimpleEntry<DayOfWeek, DayOfWeek>
            >> opTime,
        DayOfWeek dow
    ) {
        opTime
            .get(opTime.size() - 1)
            .getValue()
            .setValue(dow);
    }

    private static boolean lastEntryIsWorkday(
        List<AbstractMap.SimpleEntry<
            WorkingDayTime,
            AbstractMap.SimpleEntry<DayOfWeek, DayOfWeek>
            >> opTime
    ) {
        return opTime.get(opTime.size() - 1).getKey() != null;
    }
}
