package com.cubuanic.stepik.java.interview;

import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

public class Step2_2_06 {
    public static final String DIGITS = "0123456789";
    public static final char ADD = '+';
    public static final char SUB = '-';
    public static final char MUL = '*';
    public static final char DIV = '/';
    public static final char NEG = 'N';
    public static final char BRL = '(';
    public static final char BRR = ')';
    public static final char VAL = 'V';

    public static Deque<AbstractMap.SimpleEntry<Character, String>> tokens = new ArrayDeque<>();
    public static Deque<AbstractMap.SimpleEntry<Character, String>> grammar = new ArrayDeque<>();
    public static Deque<AbstractMap.SimpleEntry<Character, String>> rpn = new ArrayDeque<>();

    public static int calculate(String mathString) {
        tokens = new ArrayDeque<>();
        grammar = new ArrayDeque<>();
        rpn = new ArrayDeque<>();
        tokenize(mathString);
        parse();
        tokensToRPN();
        return evaluateRPN();
    }

    public static void tokenize(String expression) {
        int i = 0;
        while (i < expression.length()) {
            char c = expression.charAt(i);
            if (Arrays.asList(ADD, SUB, MUL, DIV, BRL, BRR).contains(c)) {
                tokens.add(new AbstractMap.SimpleEntry<>(c, ""));
            } else if (DIGITS.indexOf(c) >= 0) {
                StringBuilder num = new StringBuilder();
                while (i < expression.length()) {
                    c = expression.charAt(i);
                    if (DIGITS.indexOf(c) < 0) {
                        i--;
                        break;
                    }
                    num.append(c);
                    i++;
                }
                tokens.add(new AbstractMap.SimpleEntry<>(VAL, num.toString()));
            } else {
                throw new RuntimeException("Bad char '" + c + "'");
            }
            i++;
        }
    }

    /* Grammar
        E :  T1 + E  | T1 - E  | T1
        T1:  T2 * T1 | T2 / T1 | T2
        T2: -T3      | T3
        T3:  N       | (E)
     */
    public static void parse() {
        parseE();
        AbstractMap.SimpleEntry<Character, String> next = tokens.peek();
        if (next != null) {
            throw new RuntimeException("extra "
                + (next.getKey().equals(VAL) ? next.getValue() : next.getKey())
                + " after E");
        }
    }

    public static void parseE() {
        parseT1();

        AbstractMap.SimpleEntry<Character, String> next = tokens.peek();
        if (next == null) {
            return;
        }
        if (next.getKey().equals(ADD) || next.getKey().equals(SUB)) {
            grammar.add(tokens.pop());
            parseE();
        }
    }

    public static void parseT1() {
        parseT2();

        AbstractMap.SimpleEntry<Character, String> next = tokens.peek();
        if (next == null) {
            return;
        }
        if (next.getKey().equals(MUL) || next.getKey().equals(DIV)) {
            grammar.add(tokens.pop());
            parseT1();
        }
    }

    public static void parseT2() {
        AbstractMap.SimpleEntry<Character, String> next = tokens.peek();
        if (next != null && next.getKey().equals(SUB)) {
            tokens.pop();
            grammar.add(new AbstractMap.SimpleEntry<>(NEG, ""));
        }
        parseT3();
    }

    public static void parseT3() {
        AbstractMap.SimpleEntry<Character, String> next = tokens.peek();
        if (next == null) {
            throw new RuntimeException("empty T3");
        }
        if (next.getKey().equals(VAL)) {
            grammar.add(tokens.pop());
        } else if (next.getKey().equals(BRL)) {
            grammar.add(tokens.pop());
            parseE();
            next = tokens.peek();
            if (next == null) {
                throw new RuntimeException("missing )");
            }
            if (!next.getKey().equals(BRR)) {
                throw new RuntimeException("no ) after E");
            }
            grammar.add(tokens.pop());
        } else {
            throw new RuntimeException("unexpected in T3");
        }
    }

    public static void tokensToRPN() {
        Deque<AbstractMap.SimpleEntry<Character, String>> stack = new ArrayDeque<>();

        while (!grammar.isEmpty()) {
            AbstractMap.SimpleEntry<Character, String> next = grammar.pop();
            final Character op = next.getKey();
            if (op.equals(VAL)) {
                rpn.add(next);
            } else if (Arrays.asList(ADD, SUB, MUL, DIV, NEG).contains(op)) {
                while (!stack.isEmpty() && priority(stack.peek().getKey()) >= priority(op)) {
                    rpn.add(stack.pop());
                }
                stack.push(next);
            } else if (op.equals(BRL)) {
                stack.push(next);
            } else {    // BRR
                // in theory, we need to loop until !stack.isEmpty()
                // on practice we already passed grammar check, so this case could never happen
                while (true) {
                    next = stack.pop();
                    if (next.getKey().equals(BRL)) break;
                    else rpn.add(next);
                }
            }
        }
        while (!stack.isEmpty()) {
            rpn.add(stack.pop());
        }
    }

    private static int priority(Character op) {
        return op.equals(BRL) ? 0
            : op.equals(ADD) || op.equals(SUB) ? 1
            : op.equals(MUL) || op.equals(DIV) ? 2
            : 3;    // NEG
    }

    public static int evaluateRPN() {
        Deque<Integer> stack = new ArrayDeque<>();

        while (!rpn.isEmpty()) {
            final AbstractMap.SimpleEntry<Character, String> item = rpn.pop();
            final Character op = item.getKey();
            if (op.equals(VAL)) {
                stack.push(Integer.valueOf(item.getValue()));
            } else if (op.equals(ADD)) {
                stack.push(stack.pop() + stack.pop());
            } else if (op.equals(SUB)) {
                int x = stack.pop();
                stack.push(stack.pop() - x);
            } else if (op.equals(MUL)) {
                stack.push(stack.pop() * stack.pop());
            } else if (op.equals(DIV)) {
                int x = stack.pop();
                stack.push(stack.pop() / x);
            } else {    // NEG
                stack.push(-stack.pop());
            }
        }
        return stack.pop();
    }
}
