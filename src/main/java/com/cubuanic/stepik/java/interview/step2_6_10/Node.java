package com.cubuanic.stepik.java.interview.step2_6_10;

public interface Node{
    String getName();
    Node getLeft();
    Node getRight();
    Node getParent();
    void setParent(Node parent);
}
