package com.cubuanic.stepik.java.interview.step2_2_07;

public interface BigNumber extends Comparable {
    BigNumber add(BigNumber bigNumber);

    BigNumber sub(BigNumber bigNumber);
}
