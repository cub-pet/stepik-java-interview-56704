package com.cubuanic.stepik.java.interview;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Step4_1_04 {
    public static void callMethodByName(Object object, String methodName, Object... params) throws Exception {
        Arrays
            .stream(object.getClass().getDeclaredMethods())
            .filter(m -> methodName.equals(m.getName()))
            .limit(1)
            .collect(Collectors.toList())
            .get(0)
            .invoke(object, params);
    }
}
