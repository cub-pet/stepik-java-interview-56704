package com.cubuanic.stepik.java.interview.step2_6_09;

import java.util.Arrays;
import java.util.stream.Collectors;

public class NodeImpl implements Node {
    Node left;
    Node right;
    String name;

    public NodeImpl(String name) {
        this.left = null;
        this.right = null;
        this.name = name;
    }

    public NodeImpl(String name, Node left) {
        this.left = left;
        this.right = null;
        this.name = name;
    }

    public NodeImpl(String name, Node left, Node right) {
        this.left = left;
        this.right = right;
        this.name = name;
    }

    @Override
    public Node getLeft() {
        return left;
    }

    @Override
    public Node getRight() {
        return right;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        String leftName = left != null ? left.toString() : "";
        String rightName = right != null ? right.toString() : "";
        return Arrays.stream(new String[]{name, leftName, rightName})
                .filter(s -> !s.isEmpty())
                .flatMap(s -> Arrays.stream(s.split(",")))
                .sorted()
                .distinct()
                .collect(Collectors.joining(","));
    }
}
