package com.cubuanic.stepik.java.interview.step4_1_04;

import com.cubuanic.Generated;

public class A {
    final int age;
    final String name;

    public A() {
        this.age = 0;
        this.name = null;
    }

    // avoid by jacoco
    @Generated(message = "manual")
    public A(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public void sayHello() {
        System.out.println("Hello world!!!");
    }

    public void sayHello2(String whom) {
        System.out.println("Hello " + whom + "!!!");
    }
}
