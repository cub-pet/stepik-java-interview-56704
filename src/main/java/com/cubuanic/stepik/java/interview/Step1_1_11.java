package com.cubuanic.stepik.java.interview;


import com.cubuanic.stepik.java.interview.step1_1_11.Node;

public class Step1_1_11 {
    public static int treeLeafCount(Node root) {
        if (root == null) {
            return 0;
        }

        if (root.getLeft() == null && root.getRight() == null) {
            return 1;
        }

        return treeLeafCount(root.getLeft()) + treeLeafCount(root.getRight());
    }
}
