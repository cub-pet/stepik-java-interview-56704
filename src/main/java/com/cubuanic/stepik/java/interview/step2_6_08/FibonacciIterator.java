package com.cubuanic.stepik.java.interview.step2_6_08;

import java.util.Iterator;

public class FibonacciIterator implements Iterator<Integer> {
    int a = 0;
    long b = 0;

    @Override
    public boolean hasNext() {
        return b < (long) Integer.MAX_VALUE;
    }

    @Override
    public Integer next() {
        if (a == 0 && b == 0) {
            b = 1;
            return 0;
        } else {
            long result = a + b;
            a = (int) b;
            b = result;
            return a;
        }
    }
}
