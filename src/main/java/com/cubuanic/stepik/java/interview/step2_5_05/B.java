package com.cubuanic.stepik.java.interview.step2_5_05;

public class B {
    Character x;

    public B() {
        this.x = 'x';
    }

    public Character getX() {
        return x;
    }
}
