package com.cubuanic.stepik.java.interview;

import java.time.Instant;
import java.util.Date;

public class Step3_1_06 {
    public static Date instantToDate(Instant instant) {
        Instant rounded = Instant.ofEpochSecond(
                instant.getEpochSecond(),
                instant.getNano() / 1_000_000 * 1_000_000
        );

        Date result;
        try {
            result = Date.from(rounded);
        } catch (IllegalArgumentException ignored) {
            if (instant.compareTo(Instant.now()) > 0) {
                result = new Date(Long.MAX_VALUE);
            } else {
                result = new Date(Long.MIN_VALUE);
            }
        }

        return result;
    }
}
