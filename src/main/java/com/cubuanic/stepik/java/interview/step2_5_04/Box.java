package com.cubuanic.stepik.java.interview.step2_5_04;

public class Box<T> {
    private final T value;

    public Box(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
