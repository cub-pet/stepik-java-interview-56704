package com.cubuanic.stepik.java.interview.step4_3_01;

import com.cubuanic.Generated;

public class B {
    @Autowired
    C c;

    public C getC() {
        return c;
    }

    // avoid by jacoco
    @Generated(message = "manual")
    public void setC(C c) {
        this.c = c;
    }
}
