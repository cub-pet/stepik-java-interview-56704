package com.cubuanic.stepik.java.interview;

import com.cubuanic.Generated;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class Step3_4_05 {
    ReentrantLock lockA = new ReentrantLock();
    ReentrantLock lockB = new ReentrantLock();

    AtomicInteger count = new AtomicInteger(2);

//
//    Code for Stepik
//

    // avoid by jacoco
    @Generated(message = "manual")
    void method1() {
        lockA.lock();
        try {
            try {
                count.decrementAndGet();
                while (true) {
                    if (count.get() == 0) {
                        break;
                    }
                }
                lockB.lock();
                job1();
            } finally {
                lockB.unlock();
            }
        } finally {
            lockA.unlock();
        }
    }

    // avoid by jacoco
    @Generated(message = "manual")
    void method2() {
        lockB.lock();
        try {
            try {
                count.decrementAndGet();
                while (true) {
                    if (count.get() == 0) {
                        break;
                    }
                }
                lockA.lock();
                job2();
            } finally {
                lockA.unlock();
            }
        } finally {
            lockB.unlock();
        }
    }

//
//    Debugging code
//

//    // avoid by jacoco
//    @Generated(message = "manual")
//    void method1() {
//        System.out.println("Started m1");
//        System.out.println("M1 counter " + count.get());
//        lockA.lock();
//        System.out.println("M1 locked A");
//        count.decrementAndGet();
//        while (true) {
//            if (count.get() == 0) {
//                break;
//            }
//        }
//        System.out.println("M1 counter " + count.get());
//        System.out.println("Synced m1");
//        lockB.lock();
//        System.out.println("M1 locked B");
//        job1();
//        lockB.unlock();
//        System.out.println("M1 unlocked B");
//        lockA.unlock();
//        System.out.println("M1 unlocked A");
//    }
//
//    // avoid by jacoco
//    @Generated(message = "manual")
//    void method2() {
//        System.out.println("Started m2");
//        System.out.println("M2 counter " + count.get());
//        lockB.lock();
//        System.out.println("M2 locked B");
//        count.decrementAndGet();
//        while (true) {
//            if (count.get() == 0) {
//                break;
//            }
//        }
//        System.out.println("M2 counter " + count.get());
//        System.out.println("Synced m2");
//        lockA.lock();
//        System.out.println("M2 locked A");
//        job2();
//        lockA.unlock();
//        System.out.println("M2 unlocked A");
//        lockB.unlock();
//        System.out.println("M2 unlocked B");
//    }

    // avoid by jacoco
    @Generated(message = "manual")
    private void job1() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException ignored) {
        }
    }

    // avoid by jacoco
    @Generated(message = "manual")
    private void job2() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException ignored) {
        }
    }
}
