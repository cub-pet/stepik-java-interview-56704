package com.cubuanic.stepik.java.interview.step3_1_07;

import java.time.LocalTime;

public class WorkingDayTimeImpl implements WorkingDayTime{
    private LocalTime start;
    private LocalTime end;

    public WorkingDayTimeImpl(LocalTime start, LocalTime end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public LocalTime getStart() {
        return start;
    }

    @Override
    public LocalTime getEnd() {
        return end;
    }
}
