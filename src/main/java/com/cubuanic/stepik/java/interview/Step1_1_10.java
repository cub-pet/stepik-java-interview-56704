package com.cubuanic.stepik.java.interview;


import com.cubuanic.stepik.java.interview.step1_1_10.Node;

public class Step1_1_10 {
    public static int treeDepth(Node root) {
        if (root == null) {
            return 0;
        }

        return 1 + Math.max(treeDepth(root.getLeft()), treeDepth(root.getRight()));
    }
}
