package com.cubuanic.stepik.java.interview;


import com.cubuanic.stepik.java.interview.step4_2_06.Base;
import com.cubuanic.stepik.java.interview.step4_2_06.Logging;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class Step4_2_06 {

    public static Base callMethodByWeight(Base object) throws Exception {
    final Set<String> annotatedMethods = Arrays
        .stream(object.getClass().getDeclaredMethods())
        .filter(m -> m.isAnnotationPresent(Logging.class))
        .map(Method::getName)
        .collect(Collectors.toSet());

    return (Base) Proxy.newProxyInstance(
        Base.class.getClassLoader(),
        new Class[]{Base.class},
        (proxy, method, args) -> {
            Object result;
            method.setAccessible(true);
            final String methodName = method.getName();
            final boolean isLogged = annotatedMethods.contains(methodName);
            if (isLogged) {
                System.out.println("Before call " + methodName);
            }
            result = method.invoke(object, args);
            if (isLogged) {
                System.out.println("After call " + methodName);
            }
            return result;
        }
    );
    }
}
