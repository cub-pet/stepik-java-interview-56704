package com.cubuanic.stepik.java.interview.step4_2_06;

public class A implements Base {
    @Logging
    public void method1() {
        System.out.println("A method1");
    }

    public void method2() {
        System.out.println("A method2");
    }
}
