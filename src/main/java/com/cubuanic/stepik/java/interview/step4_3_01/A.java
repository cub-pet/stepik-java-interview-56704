package com.cubuanic.stepik.java.interview.step4_3_01;

import com.cubuanic.Generated;

public class A {
    @Autowired
    B b;

    C c;

    public B getB() {
        return b;
    }

    // avoid by jacoco
    @Generated(message = "manual")
    public void setB(B b) {
        this.b = b;
    }

    public C getC() {
        return c;
    }

    // avoid by jacoco
    @Generated(message = "manual")
    public void setC(C c) {
        this.c = c;
    }
}
