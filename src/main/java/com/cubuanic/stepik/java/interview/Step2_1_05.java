package com.cubuanic.stepik.java.interview;

import com.cubuanic.Generated;

public class Step2_1_05 {
    @Generated(message = "manual")
    public static void main(String[] args) {
        System.out.println(Long.valueOf(42L).equals(42L));
        System.out.println(Integer.valueOf(42) == 42);
        System.out.println(Long.valueOf(42L).equals(42));
        System.out.println(new Integer(42) == new Integer(42));
        System.out.println(Integer.valueOf(42) == Integer.valueOf(42));
    }
}
