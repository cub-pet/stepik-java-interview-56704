package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step1_2_06.Apple;
import com.cubuanic.stepik.java.interview.step1_2_06.Banana;

public class Step1_2_06 {
    public static void checkFruitCount(Object[] objects) {
        int apple = 0;
        int banana = 0;
        for (Object o : objects) {
            if (o instanceof Apple) {
                apple++;
            }
            if (o instanceof Banana) {
                banana++;
            }
        }
        System.out.println("banana=" + banana + ", apple=" + apple);
    }
}
