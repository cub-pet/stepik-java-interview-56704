package com.cubuanic.stepik.java.interview.step2_3_04;

public interface Complex {

    Complex sum(Complex other);

    Complex sub(Complex other);

    Complex mul(Complex other);

    double getReal();

    double getImage();
}
