package com.cubuanic.stepik.java.interview.step4_3_02;

@FunctionalInterface
public interface Operation {
    void call() throws Exception;
}