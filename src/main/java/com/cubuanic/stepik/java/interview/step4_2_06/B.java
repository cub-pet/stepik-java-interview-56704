package com.cubuanic.stepik.java.interview.step4_2_06;

public class B implements Base {
    public void method1() {
        System.out.println("B method1");
    }

    @Logging
    public void method2() {
        System.out.println("B method2");
    }
}
