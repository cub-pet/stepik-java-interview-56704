create table books
(
    id   serial
        constraint books_pk primary key,
    name varchar
);

create table sale_points
(
    id   serial
        constraint sale_points_pk primary key,
    name varchar
);


create table audit
(
    id_sale_point int references sale_points (id),
    id_book       int references books (id),
    count_books   int
);

insert into books (name)
values ('Книга рецептов');
insert into books (name)
values ('Гоблины в подземелье');
insert into books (name)
values ('Дьявольска вишенка');
insert into books (name)
values ('Такой крыжовник');
insert into books (name)
values ('Приключение картошечки');
insert into books (name)
values ('Приключение помидорки');

insert into sale_points (name)
values ('Солнышко');
insert into sale_points (name)
values ('Марс');
insert into sale_points (name)
values ('Земля');
insert into sale_points (name)
values ('Меркурий');

insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Солнышко') s,
     (select id as id_book from books where name = 'Книга рецептов') b,
     (select 5 as count_books) c;
insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Солнышко') s,
     (select id as id_book from books where name = 'Гоблины в подземелье') b,
     (select 30 as count_books) c;
insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Марс') s,
     (select id as id_book from books where name = 'Дьявольска вишенка') b,
     (select 11 as count_books) c;
insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Солнышко') s,
     (select id as id_book from books where name = 'Такой крыжовник') b,
     (select 6 as count_books) c;
insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Марс') s,
     (select id as id_book from books where name = 'Приключение картошечки') b,
     (select 20 as count_books) c;
insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Земля') s,
     (select id as id_book from books where name = 'Приключение картошечки') b,
     (select 1 as count_books) c;
insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Меркурий') s,
     (select id as id_book from books where name = 'Приключение картошечки') b,
     (select 1 as count_books) c;
insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Солнышко') s,
     (select id as id_book from books where name = 'Приключение помидорки') b,
     (select 5 as count_books) c;
insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Земля') s,
     (select id as id_book from books where name = 'Приключение помидорки') b,
     (select 7 as count_books) c;
insert into audit
select s.id_sale_point, b.id_book, c.count_books
from (select id as id_sale_point from sale_points where name = 'Меркурий') s,
     (select id as id_book from books where name = 'Приключение помидорки') b,
     (select 7 as count_books) c;

select s.name as sale_point, b.name as book, a.count_books as count_books
from sale_points s,
     books b,
     audit a
where s.id = a.id_sale_point
  and b.id = a.id_book
order by s.name, b.name;