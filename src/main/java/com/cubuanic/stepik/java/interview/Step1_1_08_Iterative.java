package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step1_1_08.Node;

public class Step1_1_08_Iterative {
    public static Node reverse(Node head) {
        if (head != null && head.getNext() != null) {
            Node tmp;
            Node tail = head.getNext();
            head.setNext(null);

            while (tail != null) {
                tmp = tail;
                tail = tail.getNext();
                tmp.setNext(head);
                head = tmp;
            }
        }
        return head;
    }
}