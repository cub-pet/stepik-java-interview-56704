package com.cubuanic.stepik.java.interview.step2_6_08;

import java.util.Iterator;

public class FibonacciIterable implements Iterable<Integer> {
    @Override
    public Iterator<Integer> iterator() {
        return new FibonacciIterator();
    }
}