package com.cubuanic.stepik.java.interview;

import java.util.Scanner;

public class Step1_1_06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        calculateAndPrint(scanner.nextInt());
    }

    private static void calculateAndPrint(int n) {
        if ((n / 100) % 2 == 0) {
            System.out.println(n + 100);
        } else {
            System.out.println(n * 2);
        }
    }
}
