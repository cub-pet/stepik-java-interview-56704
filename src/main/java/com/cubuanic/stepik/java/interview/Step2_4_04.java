package com.cubuanic.stepik.java.interview;

import com.cubuanic.Generated;

public class Step2_4_04 {
    // avoid by jacoco
    @Generated(message = "manual")
    public static void generateOutOfMemoryError() {
        int[] x = new int[Integer.MAX_VALUE / 2];
        x[0] = 1 + x[Integer.MAX_VALUE / 2];
        generateOutOfMemoryError();
    }
}

