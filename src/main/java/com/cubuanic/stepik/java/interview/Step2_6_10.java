package com.cubuanic.stepik.java.interview;


import com.cubuanic.stepik.java.interview.step2_6_10.Node;

import java.util.List;

public class Step2_6_10 {
    public static void setParents(List<Node> nodes) {
        for (Node node : nodes) {
            if (node.getLeft() != null) {
                node.getLeft().setParent(node);
            }
            if (node.getRight() != null) {
                node.getRight().setParent(node);
            }
        }
    }

    public static void resetParents(List<Node> nodes) {
        for (Node item : nodes) {
            item.setParent(null);
        }
    }
}
