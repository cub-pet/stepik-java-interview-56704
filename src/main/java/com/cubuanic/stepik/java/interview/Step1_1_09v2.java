package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step1_1_09.Node;

public class Step1_1_09v2 {
    public static Node merge(Node head1, Node head2) {
        if (head1 == null) {
            return head2;
        }
        if (head2 == null) {
            return head1;
        }

        if (head1.getData() > head2.getData()) {
            Node tmp = head1;
            head1 = head2;
            head2 = tmp;
        }

        Node head = head1;

        while (head1.getNext() != null) {
            while (head1.getNext() != null && head1.getNext().getData() <= head2.getData()) {
                head1 = head1.getNext();
            }
            if (head1.getNext() != null) {
                Node tmp = head1.getNext();
                head1.setNext(head2);
                head1 = head2;
                head2 = tmp;
            }
        }

        head1.setNext(head2);

        return head;
    }

}
