package com.cubuanic.stepik.java.interview;

public class Step1_1_07 {
    public static void checkSortArrayLength(int[] array) {
        if (array.length < 2) {
            System.out.println(1);
            return;
        }

        int result = 1;
        int series = 1;
        int idx = 0;
        do {
            idx++;
            if (array[idx - 1] <= array[idx]) {
                series++;
                if (series > result) {
                    result = series;
                }
            } else {
                series = 1;
            }
        } while (idx < array.length - 1);

        System.out.println(result);
    }
}
