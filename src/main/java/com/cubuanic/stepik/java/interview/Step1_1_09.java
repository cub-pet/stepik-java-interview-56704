package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step1_1_09.Node;

public class Step1_1_09 {
    public static Node merge(Node head1, Node head2) {
        if (head1 == null) {
            return head2;
        }
        if (head2 == null) {
            return head1;
        }

        Node head = null;
        Node prev = null;

        while (head1 != null && head2 != null) {
            Node min;
            if (head1.getData() < head2.getData()) {
                min = head1;
                head1 = head1.getNext();
            } else {
                min = head2;
                head2 = head2.getNext();
            }

            if (head == null) {
                head = min;
            }

            if (prev != null) {
                prev.setNext(min);
            }
            prev = min;
        }

        if (head1 != null) {
            prev.setNext(head1);
        }

        if (head2 != null) {
            prev.setNext(head2);
        }

        return head;
    }
}
