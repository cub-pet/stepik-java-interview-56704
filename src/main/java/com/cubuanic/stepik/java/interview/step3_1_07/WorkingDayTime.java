package com.cubuanic.stepik.java.interview.step3_1_07;

import java.time.LocalTime;

public interface WorkingDayTime {
    LocalTime getStart();
    LocalTime getEnd();
}
