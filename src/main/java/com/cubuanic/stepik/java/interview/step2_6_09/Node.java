package com.cubuanic.stepik.java.interview.step2_6_09;

public interface Node {
    Node getLeft();

    Node getRight();

    String getName();
}
