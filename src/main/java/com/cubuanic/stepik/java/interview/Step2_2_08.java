package com.cubuanic.stepik.java.interview;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Step2_2_08 {

    public static void findAllUrl(String text) {
        Matcher matcher = Pattern.compile("(https?://[^ \\n]+)").matcher(text);
        while (matcher.find()) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                System.out.println(text.substring(matcher.start(i), matcher.end(i)));
            }
        }
    }
}
