package com.cubuanic.stepik.java.interview;

public class Step2_1_08 {
    public static int reverse(int n) {
        int result = 0;
        boolean negative = false;

        if (n < 0) {
            negative = true;
            n = -n;
        }

        while (n > 0) {
            result = result * 10 + n % 10;
            n /= 10;
        }

        return negative ? -result : result;
    }
}
