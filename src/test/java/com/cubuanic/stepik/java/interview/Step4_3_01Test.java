package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step4_3_01.A;
import com.cubuanic.stepik.java.interview.step4_3_01.B;
import com.cubuanic.stepik.java.interview.step4_3_01.C;
import com.cubuanic.stepik.java.interview.step4_3_01.CandidateNotFindException;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cubuanic.TestUtils.*;
import static com.cubuanic.stepik.java.interview.Step4_3_01.dependencyInjection;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(Enclosed.class)
public class Step4_3_01Test {
    public static class DependencyInjectionSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class DependencyInjectionParameterizedTests {
        private String name;
        private List<Object> objects;
        private String expected;
        private Consumer<List<Object>> checkWires;

        public DependencyInjectionParameterizedTests(
            String name,
            List<Object> objects,
            String expected,
            Consumer<List<Object>> checkWires
        ) {
            this.name = name;
            this.objects = objects;
            this.expected = expected;
            this.checkWires = checkWires;
        }

        @Parameterized.Parameters
        public static <T> Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {
                    "case 1",
                    Arrays.asList(new A(), new B(), new C()),
                    "good",
                    (Consumer<List<Object>>) DependencyInjectionParameterizedTests::checkWiresCase1
                },
                {
                    "case 2",
                    Arrays.asList(new A(), new B()),
                    CandidateNotFindException.class.getSimpleName(),
                    null
                },
            });
        }

        public static void checkWiresCase1(List<Object> objects) {
            A a = (A) objects.stream().filter(o -> o instanceof A).collect(Collectors.toList()).get(0);
            B b = (B) objects.stream().filter(o -> o instanceof B).collect(Collectors.toList()).get(0);
            C c = (C) objects.stream().filter(o -> o instanceof C).collect(Collectors.toList()).get(0);

            // for a
            assertEquals(b, a.getB());
            assertNull(a.getC());

            // for b
            assertEquals(c, b.getC());

            // for c
            // nothing, C has no fields
        }

        @Test(timeout = 5000)
        public void testDependencyInjection() throws Throwable {
            runOutputTest(
                ignore -> {
                    try {
                        dependencyInjection(objects);
                        System.out.println("good");
                        checkWires.accept(objects);
                    } catch (Exception e) {
                        System.out.println(e.getClass().getSimpleName());
                    }
                },
                null,
                name,
                expected
            );
        }

    }
}
