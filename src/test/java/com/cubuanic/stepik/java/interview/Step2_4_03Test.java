package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_4_03.generateStackOverflowError;

public class Step2_4_03Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000, expected = StackOverflowError.class)
    public void testGenerateStackOverflowError() {
        generateStackOverflowError();
    }
}