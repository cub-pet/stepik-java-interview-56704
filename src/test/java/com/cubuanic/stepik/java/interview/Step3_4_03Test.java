package com.cubuanic.stepik.java.interview;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class Step3_4_03Test {
    public static class SafeIncrementSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class SafeIncrementParameterizedTests implements Runnable {
        private int threads;
        private int limit;
        private Step3_4_03 clazz;

        public SafeIncrementParameterizedTests(int threads, int limit) {
            this.threads = threads;
            this.limit = limit;
            this.clazz = new Step3_4_03();
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {1, 2000},
                {3, 2000},
                {1, 10000},
                {5, 10000},
                {50, 50000},
            });
        }

        @Test(timeout = 5000)
        public void testSafeIncrement() throws InterruptedException {
            assertEquals(
                "Start : " + threads + "*" + limit,
                0,
                clazz.getCount()
            );
            runThreads(threads, limit);
            assertEquals(
                "Result : " + threads + "*" + limit,
                threads * limit,
                clazz.getCount()
            );
        }

        private void runThreads(int threadsCnt, int limit) throws InterruptedException {
            List<Thread> threads = new ArrayList<>(threadsCnt);
            for (int i = 0; i < threadsCnt; i++) {
                final Thread thread = new Thread(this);
                threads.add(thread);
                thread.start();
            }
            for (int i = 0; i < threadsCnt; i++) {
                Thread thread = threads.get(i);
                thread.join();
            }
        }

        @Override
        public void run() {
            for (int i = 0; i < limit; i++) {
                clazz.safeIncrement();
            }
        }
    }
}