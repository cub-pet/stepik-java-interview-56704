package com.cubuanic.stepik.java.interview;

import org.junit.Ignore;
import org.junit.Test;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Step3_4_05Test {
    private Step3_4_05 clazz = new Step3_4_05();
    private AtomicBoolean deadlocked = new AtomicBoolean(false);

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testDeadlock() {
        DeadlockDetector deadlockDetector = new DeadlockDetector(
            new DeadlockConsoleHandler(),
            1,
            TimeUnit.SECONDS
        );
        deadlockDetector.start();

        final AbstractMap.SimpleEntry<Thread, Thread> threads = runThreads();
        while (true) {
            if (deadlocked.get()) {
                break;
            }
        }
        assertTrue(deadlocked.get());
        threads.getKey().interrupt();
        threads.getValue().interrupt();
    }

    private AbstractMap.SimpleEntry<Thread, Thread> runThreads() {
        Thread t1 = new Thread(() -> clazz.method1());
        Thread t2 = new Thread(() -> clazz.method2());
        t1.start();
        t2.start();

        return new AbstractMap.SimpleEntry<>(t1, t2);
    }

    // Code below is based on https://dzone.com/articles/how-detect-java-deadlocks
    public interface DeadlockHandler {
        void handleDeadlock(final ThreadInfo[] deadlockedThreads);
    }

    public static class DeadlockDetector {

        private final DeadlockHandler deadlockHandler;
        private final long period;
        private final TimeUnit unit;
        private final ThreadMXBean mbean = ManagementFactory.getThreadMXBean();
        private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

        final Runnable deadlockCheck = () -> {
            long[] deadlockedThreadIds = DeadlockDetector.this.mbean.findDeadlockedThreads();

            if (deadlockedThreadIds != null) {
                System.out.printf("Deadlock detected");
                ThreadInfo[] threadInfos =
                    DeadlockDetector.this.mbean.getThreadInfo(deadlockedThreadIds);

                DeadlockDetector.this.deadlockHandler.handleDeadlock(threadInfos);
            }
        };

        public DeadlockDetector(final DeadlockHandler deadlockHandler,
                                final long period, final TimeUnit unit) {
            this.deadlockHandler = deadlockHandler;
            this.period = period;
            this.unit = unit;
        }

        public void start() {
            this.scheduler.scheduleAtFixedRate(
                this.deadlockCheck, this.period, this.period, this.unit);
        }
    }

    public class DeadlockConsoleHandler implements DeadlockHandler {

        @Override
        public void handleDeadlock(final ThreadInfo[] deadlockedThreads) {
            if (deadlockedThreads != null) {
                Step3_4_05Test.this.deadlocked.set(true);
            }
        }
    }
}