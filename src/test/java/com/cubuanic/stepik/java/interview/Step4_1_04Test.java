package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step4_1_04.A;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;
import static com.cubuanic.stepik.java.interview.Step4_1_04.callMethodByName;

@RunWith(Enclosed.class)
public class Step4_1_04Test {
    public static class CallMethodByNameSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class CallMethodByNameParameterizedTests {
        private Object object;
        private String methodName;
        private Object[] params;
        private String expected;

        public CallMethodByNameParameterizedTests(Object object, String methodName, Object[] params, String expected) {
            this.object = object;
            this.methodName = methodName;
            this.params = params;
            this.expected = expected;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            Object[] p1 = new Object[0];
            Object[] p2 = new Object[]{"the best world"};
            return Arrays.asList(new Object[][]{
                {new A(), "sayHello", p1, "Hello world!!!"},
                {new A(), "sayHello2", p2, "Hello the best world!!!"},
            });
        }

        @Test(timeout = 5000)
        public void testCallMethodByName() throws Throwable {
            runOutputTest(
                ignore -> {
                    try {
                        callMethodByName(object, methodName, params);
                    } catch (Exception ignored) {
                        // ignored
                    }
                },
                null,
                object.getClass().getSimpleName() + "." + methodName,
                expected);
        }
    }
}