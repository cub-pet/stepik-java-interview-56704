package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;
import static com.cubuanic.stepik.java.interview.Step1_1_07.checkSortArrayLength;

public class Step1_1_07Test {
    List<AbstractMap.SimpleEntry<List<Integer>, String>> testData = Arrays.asList(
        new AbstractMap.SimpleEntry<>(Arrays.asList(1), "1"),
        new AbstractMap.SimpleEntry<>(Arrays.asList(2, 1), "1"),
        new AbstractMap.SimpleEntry<>(Arrays.asList(2, 1, 4), "2"),
        new AbstractMap.SimpleEntry<>(Arrays.asList(2, 1, 4, 1, 2, 3), "3"),
        new AbstractMap.SimpleEntry<>(Arrays.asList(2, 1, 4, 1, 2, 3, 4, 5, 6, 7, 3, 1), "7")
    );

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testCheckSortArrayLength() throws Throwable {
        for (final AbstractMap.SimpleEntry<List<Integer>, String> entry : testData) {
            List<Integer> input = entry.getKey();
            int[] inputArray = input.stream().mapToInt(i -> i).toArray();
            runOutputTest(
                ints -> checkSortArrayLength(ints),
                inputArray,
                Arrays.toString(inputArray),
                entry.getValue());
        }
    }
}
