package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_1_09.reverseByte;
import static org.junit.Assert.assertEquals;

public class Step2_1_09Test {
    List<AbstractMap.SimpleEntry<Integer, Integer>> testData = Arrays.asList(
        new AbstractMap.SimpleEntry<>(0, 0),
        new AbstractMap.SimpleEntry<>(0xfe01ccd1, 0xd1cc01fe)
    );

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testReverse() {
        for (final AbstractMap.SimpleEntry<Integer, Integer> entry : testData) {
            assertEquals((int) entry.getValue(), reverseByte(entry.getKey()));
        }
    }
}