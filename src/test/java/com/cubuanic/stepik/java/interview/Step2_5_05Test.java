package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step2_5_05.A;
import com.cubuanic.stepik.java.interview.step2_5_05.B;
import com.cubuanic.stepik.java.interview.step2_5_05.FactoryA;
import com.cubuanic.stepik.java.interview.step2_5_05.FactoryB;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class Step2_5_05Test {
    @Test(timeout = 5000)
    public void testGenerateInstance() throws Exception {
        FactoryA fa = new FactoryA();
        A a = fa.generateInstance();
        assertNotNull("A not null", a);
        assertEquals("class A", A.class, a.getClass());
        assertEquals("A getter", (Integer) 1, a.getA());

        FactoryB fb = new FactoryB();
        B b = fb.generateInstance();
        assertNotNull("B not null", b);
        assertEquals("class B", B.class, b.getClass());
        assertEquals("B getter", (Character) 'x', b.getX());
    }
}