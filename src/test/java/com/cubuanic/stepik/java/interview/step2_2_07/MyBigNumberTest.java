package com.cubuanic.stepik.java.interview.step2_2_07;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.runners.Parameterized.Parameters;

@RunWith(Enclosed.class)
public class MyBigNumberTest {
    public static class MyBigNumberSingleTests {
        @Test(timeout = 5000, expected = NullPointerException.class)
        public void testNull() {
            int fake = new MyBigNumber("1").compareTo(null);
        }

        @Test(timeout = 5000, expected = ArithmeticException.class)
        public void testWrongObject() {
            int fake = new MyBigNumber("1").compareTo(Long.valueOf(1));
        }
    }

    @RunWith(Parameterized.class)
    public static class MyBigNumberParameterizedTest {
        private MyBigNumber a;
        private MyBigNumber b;
        private MyBigNumber add;
        private MyBigNumber addadd;
        private MyBigNumber sub;
        private MyBigNumber subsub;
        private int cmp;

        public MyBigNumberParameterizedTest(String a, String b, String add, String addadd, String sub, String subsub, int cmp) {
            this.a = new MyBigNumber(a);
            this.b = new MyBigNumber(b);
            this.add = new MyBigNumber(add);
            this.addadd = new MyBigNumber(addadd);
            this.sub = new MyBigNumber(sub);
            this.subsub = new MyBigNumber(subsub);
            this.cmp = cmp;
        }

        @Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {
                            "00",
                            "0",
                            "0",
                            "0",
                            "0",
                            "0",
                            0
                    },
                    {
                            "1",
                            "1",
                            "2",
                            "3",
                            "0",
                            "-1",
                            0
                    },
                    {
                            "1",
                            "2",
                            "3",
                            "5",
                            "-1",
                            "-3",
                            -1
                    },
                    {
                            "999_999_999_999_999_999",
                            "999_999_999_999_999_999",
                            "1_999_999_999_999_999_998",
                            "2_999_999_999_999_999_997",
                            "0",
                            "-999_999_999_999_999_999",
                            0
                    },
                    {
                            "999_999_999_999_999_999",
                            "1_999_999_999_999_999_999",
                            "2_999_999_999_999_999_998",
                            "4_999_999_999_999_999_997",
                            "-1_000_000_000_000_000_000",
                            "-2_999_999_999_999_999_999",
                            -1
                    },
                    {
                            "1_000_000_000_000_000_000",
                            "999_999_999_999_999_999",
                            "1_999_999_999_999_999_999",
                            "2_999_999_999_999_999_998",
                            "1",
                            "-999_999_999_999_999_998",
                            1
                    },
                    {
                            "2_000_000_000_000_000_000",
                            "999_999_999_999_999_999",
                            "2_999_999_999_999_999_999",
                            "3_999_999_999_999_999_998",
                            "1000000000000000001",
                            "2",
                            1
                    },
                    {
                            "2_000_000_000_000_000_002",
                            "999_999_999_999_999_999",
                            "3_000_000_000_000_000_001",
                            "4_000_000_000_000_000_000",
                            "1_000_000_000_000_000_003",
                            "4",
                            1
                    },
                    {
                            "-38027450742057608221309764383410169802626",
                            "0",
                            "-38027450742057608221309764383410169802626",
                            "-38027450742057608221309764383410169802626",
                            "-38027450742057608221309764383410169802626",
                            "-38027450742057608221309764383410169802626",
                            -1
                    },
                    {
                            "38027450742057608221309764383410169802626",
                            "0",
                            "38027450742057608221309764383410169802626",
                            "38027450742057608221309764383410169802626",
                            "38027450742057608221309764383410169802626",
                            "38027450742057608221309764383410169802626",
                            1
                    },
                    {
                            "0",
                            "38027450742057608221309764383410169802626",
                            "38027450742057608221309764383410169802626",
                            "76054901484115216442619528766820339605252",
                            "-38027450742057608221309764383410169802626",
                            "-76054901484115216442619528766820339605252",
                            -1
                    },
                    {
                            "0",
                            "-38027450742057608221309764383410169802626",
                            "-38027450742057608221309764383410169802626",
                            "-76054901484115216442619528766820339605252",
                            "38027450742057608221309764383410169802626",
                            "76054901484115216442619528766820339605252",
                            1
                    },
                    {
                            "0",
                            "38027450742057608221309764383410169802626",
                            "38027450742057608221309764383410169802626",
                            "76054901484115216442619528766820339605252",
                            "-38027450742057608221309764383410169802626",
                            "-76054901484115216442619528766820339605252",
                            -1
                    },
                    {
                            "0",
                            "99999999999999999999999999999999999",
                            "99999999999999999999999999999999999",
                            "199999999999999999999999999999999998",
                            "-99999999999999999999999999999999999",
                            "-199999999999999999999999999999999998",
                            -1
                    },
                    {
                            "0",
                            "-99999999999999999999999999999999999",
                            "-99999999999999999999999999999999999",
                            "-199999999999999999999999999999999998",
                            "99999999999999999999999999999999999",
                            "199999999999999999999999999999999998",
                            1
                    },
                    {
                            "-38027450742057608221309764383410169802626",
                            "-38027450742057608221309764383410169802626",
                            "-76054901484115216442619528766820339605252",
                            "-114082352226172824663929293150230509407878",
                            "0",
                            "38027450742057608221309764383410169802626",
                            0
                    },
                    {
                            "20504962648058829508634537240139148734",
                            "469915985327887",
                            "20504962648058829508635007156124476621",
                            "20504962648058829508635477072109804508",
                            "20504962648058829508634067324153820847",
                            "20504962648058829508633597408168492960",
                            1
                    },
                    {
                            "-20504962648058829508634537240139148734",
                            "-469915985327887",
                            "-20504962648058829508635007156124476621",
                            "-20504962648058829508635477072109804508",
                            "-20504962648058829508634067324153820847",
                            "-20504962648058829508633597408168492960",
                            -1
                    }
            });
        }

        @Test(timeout = 5000)
        public void testAdd() {
            assertEquals(
                    a.toString() + " add " + b.toString(),
                    add.toString(),
                    a.add(b).toString()
            );
        }

        @Test(timeout = 5000)
        public void testAddAdd() {
            assertEquals(
                    a.toString() + " addadd " + b.toString(),
                    addadd.toString(),
                    a.add(b).add(b).toString()
            );
        }

        @Test(timeout = 5000)
        public void testSub() {
            assertEquals(
                    a.toString() + " sub " + b.toString(),
                    sub.toString(),
                    a.sub(b).toString()
            );
        }

        @Test(timeout = 5000)
        public void testSubSub() {
            assertEquals(
                    a.toString() + " subsub " + b.toString(),
                    subsub.toString(),
                    a.sub(b).sub(b).toString()
            );
        }

        @Test(timeout = 5000)
        public void testCmp() {
            assertEquals(
                    a.toString() + " cmp " + b.toString(),
                    cmp,
                    a.compareTo(b)
            );
        }
    }
}