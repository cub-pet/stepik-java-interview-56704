package com.cubuanic.stepik.java.interview.step2_6_09;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static org.junit.Assert.assertEquals;
import static org.junit.runners.Parameterized.Parameters;

@RunWith(Enclosed.class)
public class TreeIteratorTest {
    public static Node f = new NodeImpl("f");
    public static Node e = new NodeImpl("e");
    public static Node d = new NodeImpl("d");
    public static Node c = new NodeImpl("c", e, f);
    public static Node b = new NodeImpl("b", d);
    public static Node a = new NodeImpl("a", b, c);

    @RunWith(Parameterized.class)
    public static class TreeIteratorToStringTests {
        private String message;
        private String expected;
        private Node tree;

        public TreeIteratorToStringTests(String message, String expected, Node tree) {
            this.message = message;
            this.expected = expected;
            this.tree = tree;
        }

        @Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {"toStringF", "f", f},
                    {"toStringC", "c,e,f", c},
                    {"toStringB", "b,d", b},
                    {"toStringA", "a,b,c,d,e,f", a},
            });
        }

        @Test(timeout = 5000)
        public void testToString() {
            assertEquals(message, expected, tree.toString());
        }
    }

    @RunWith(Parameterized.class)
    public static class TreeIteratorParameterizedTest {

        private String message;
        private String expected;
        private Node tree;

        public TreeIteratorParameterizedTest(String message, String expected, Node tree) {
            this.message = message;
            this.expected = expected;
            this.tree = tree;
        }

        @Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {"walkF", "f", f},
                    {"walkC", "c,e,f", c},
                    {"walkB", "b,d", b},
                    {"walkA", "a,b,c,d,e,f", a},
            });
        }

        @Test(timeout = 5000)
        public void testTreeIterator() {
            TreeSet<String> result = new TreeSet<>();
            for (TreeIterator it = new TreeIterator(tree); it.hasNext(); ) {
                result.add(it.next().getName());
            }
            assertEquals(
                    message,
                    expected,
                    String.join(",", result.toArray(new String[0]))
            );
        }
    }
}