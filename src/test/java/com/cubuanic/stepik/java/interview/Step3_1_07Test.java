package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step3_1_07.WorkingDayTime;
import com.cubuanic.stepik.java.interview.step3_1_07.WorkingDayTimeImpl;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step3_1_07.getOperationTime;
import static java.util.Arrays.stream;
import static java.util.Locale.forLanguageTag;
import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class Step3_1_07Test {
    public static class GetOperationTimeSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class GetOperationTimeParameterizedTests {
        private String from;
        private String to;

        public GetOperationTimeParameterizedTests(String from, String to) {
            this.from = from;
            this.to = to;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {
                    "пн 09:00-20:00\n" +
                        "вт 09:00-20:00\n" +
                        "ср 09:00-20:00\n" +
                        "чт 09:00-20:00\n" +
                        "пт 09:00-20:00",
                    "пн-пт 09:00-20:00, сб, вс выходной"
                },
                {
                    "пн 09:00-20:00\n" +
                        "вт 09:00-20:00\n" +
                        "ср 09:00-20:00\n" +
                        "чт 09:00-20:00\n" +
                        "пт 09:00-21:00",
                    "пн-чт 09:00-20:00, пт 09:00-21:00, сб, вс выходной"
                },
                {
                    "пн 09:00-20:00\n" +
                        "вт 09:00-20:00\n" +
                        "ср 09:00-20:00\n" +
                        "пт 09:00-20:00\n" +
                        "сб 10:00-19:00",
                    "пн-ср 09:00-20:00, чт выходной, пт 09:00-20:00, сб 10:00-19:00, вс выходной"
                },
            });
        }

        @Test(timeout = 5000)
        public void testGetOperationTime() {
            assertEquals(
                to,
                getOperationTime(getDayTimeMap(from))
            );
        }

        private Map<DayOfWeek, WorkingDayTime> getDayTimeMap(String from) {
            return stream(from.split("\n"))
                .map(s -> {
                    final String[] dayMap = s.split(" ");
                    DayOfWeek dow = DayOfWeek.from(
                        DateTimeFormatter
                            .ofPattern("EE", forLanguageTag("ru"))
                            .parse(dayMap[0])
                    );
                    final String[] wdtParts = dayMap[1].split("-");
                    final WorkingDayTime wdt = new WorkingDayTimeImpl(
                        strToLocalTime(wdtParts[0]),
                        strToLocalTime(wdtParts[1])
                    );
                    return new AbstractMap.SimpleEntry<>(dow, wdt);
                })
                .collect(Collectors.toMap(
                    AbstractMap.SimpleEntry::getKey,
                    AbstractMap.SimpleEntry::getValue
                ));
        }

        private LocalTime strToLocalTime(String time) {
            return LocalTime.parse(time);
        }
    }
}