package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step1_1_08.Node;
import com.cubuanic.stepik.java.interview.step1_1_08.NodeImpl;
import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step1_1_08_Iterative.reverse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class Step1_1_08_IterativeTest {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void test1() {
        assertNull(reverse(null));
    }

    @Test(timeout = 5000)
    public void test2() {
        Node q = new NodeImpl();
        assertEquals(reverse(q), q);
    }

    @Test(timeout = 5000)
    public void test3() {
        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        a.setNext(b);
        assertEquals("a,b", a.toString());
        assertEquals("b,a", reverse(a).toString());
    }

    @Test(timeout = 5000)
    public void test4() {
        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        a.setNext(b);
        b.setNext(c);
        c.setNext(d);
        assertEquals("a,b,c,d", a.toString());
        assertEquals("d,c,b,a", reverse(a).toString());
    }
}
