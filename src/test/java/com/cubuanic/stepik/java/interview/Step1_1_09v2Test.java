package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step1_1_09.Node;
import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step1_1_09v2.merge;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class Step1_1_09v2Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void test1() {
        assertNull(merge(null, null));
    }

    @Test(timeout = 5000)
    public void test2() {
        Node a = new Node(1);
        Node b = new Node(1);
        assertEquals("1,1", merge(a, b).toString());
    }

    @Test(timeout = 5000)
    public void test3() {
        Node a = new Node(2);
        Node b = new Node(3);
        assertEquals("2,3", merge(a, b).toString());
    }

    @Test(timeout = 5000)
    public void test4() {
        Node a = new Node(2);
        Node b = new Node(3);
        assertEquals("2,3", merge(b, a).toString());
    }

    @Test(timeout = 5000)
    public void test5() {
        Node a = new Node(1);
        Node b = new Node();
        b.setData(3);
        a.setNext(b);

        assertEquals("1,3", a.toString());
        assertEquals("1,3", merge(a, null).toString());
    }

    @Test(timeout = 5000)
    public void test6() {
        Node a = new Node(1);
        Node b = new Node(3);
        a.setNext(b);

        assertEquals("1,3", a.toString());
        assertEquals("1,3", merge(null, a).toString());
    }

    @Test(timeout = 5000)
    public void test7() {
        Node a = new Node(1);
        Node b = new Node(3);
        a.setNext(b);

        Node c = new Node(2);
        Node d = new Node(4);
        c.setNext(d);

        assertEquals("1,3", a.toString());
        assertEquals("2,4", c.toString());
        assertEquals("1,2,3,4", merge(a, c).toString());
    }

    @Test(timeout = 5000)
    public void test8() {
        Node a = new Node(1);
        Node b = new Node(3);
        a.setNext(b);

        Node c = new Node(2);
        Node d = new Node(4);
        c.setNext(d);

        assertEquals("1,3", a.toString());
        assertEquals("2,4", c.toString());
        assertEquals("1,2,3,4", merge(c, a).toString());
    }

    @Test(timeout = 5000)
    public void test9() {
        Node a1 = new Node(1);
        Node a2 = new Node(2);
        Node a3 = new Node(3);
        Node a4 = new Node(3);
        a1.setNext(a2);
        a2.setNext(a3);
        a3.setNext(a4);

        Node b1 = new Node(2);
        Node b2 = new Node(4);
        Node b3 = new Node(4);
        Node b4 = new Node(4);
        b1.setNext(b2);
        b2.setNext(b3);
        b3.setNext(b4);

        assertEquals("1,2,3,3", a1.toString());
        assertEquals("2,4,4,4", b1.toString());
        assertEquals("1,2,2,3,3,4,4,4", merge(b1, a1).toString());
    }

    @Test(timeout = 5000)
    public void test10() {
        Node a1 = new Node(1);
        Node a2 = new Node(2);
        Node a3 = new Node(7);
        Node a4 = new Node(8);
        a1.setNext(a2);
        a2.setNext(a3);
        a3.setNext(a4);

        Node b1 = new Node(3);
        Node b2 = new Node(4);
        Node b3 = new Node(5);
        Node b4 = new Node(6);
        b1.setNext(b2);
        b2.setNext(b3);
        b3.setNext(b4);

        assertEquals("1,2,3,4,5,6,7,8", merge(b1, a1).toString());
    }
}
