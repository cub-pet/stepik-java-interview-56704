package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;
import static com.cubuanic.stepik.java.interview.Step2_2_08.findAllUrl;

public class Step2_2_08Test {
    public String input = "rhrthr https://stepik.org edtrhert r113223@12 https://wererwg.ru erwg3rg@123009https\n" +
            "rhrthr http://sfrgeee.org/ewfef/wqewrg\n" +
            "5476y65yyyyyh$%&$\n" +
            "https://makedreamprofits.ru/?utm_source=ewqee&utm_medium=qew";

    public String output = "https://stepik.org\n" +
            "https://wererwg.ru\n" +
            "http://sfrgeee.org/ewfef/wqewrg\n" +
            "https://makedreamprofits.ru/?utm_source=ewqee&utm_medium=qew";

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testFindAllUrl() throws Throwable {
        runOutputTest(
                text -> findAllUrl(text),
                input,
                input,
                output);
    }
}