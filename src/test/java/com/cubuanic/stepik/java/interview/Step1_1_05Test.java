package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_1_05Test {
    List<AbstractMap.SimpleEntry<String, String>> testData = Arrays.asList(
        new AbstractMap.SimpleEntry<>("0", "0"),
        new AbstractMap.SimpleEntry<>("1", "1"),
        new AbstractMap.SimpleEntry<>("2", "1"),
        new AbstractMap.SimpleEntry<>("6", "8"),
        new AbstractMap.SimpleEntry<>("7", "13"),
        new AbstractMap.SimpleEntry<>("8", "21"),
        new AbstractMap.SimpleEntry<>("9", "34"),
        new AbstractMap.SimpleEntry<>("10", "55"),
        new AbstractMap.SimpleEntry<>("11", "89")
    );

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testFib() throws Throwable {
        for (final AbstractMap.SimpleEntry<String, String> entry : testData) {
            runInputOutputTest(entry.getKey(), entry.getValue());
        }
    }
}
