package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import static com.cubuanic.TestUtils.assertOutputEquals;
import static com.cubuanic.TestUtils.checkClassInstantiation;

public class Step2_1_05Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testMain() throws Throwable {
        assertOutputEquals(
                "true\n" +
                        "true\n" +
                        "false\n" +
                        "false\n" +
                        "true"
        );
    }

}