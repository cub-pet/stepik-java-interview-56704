package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_1_06Test {
    List<AbstractMap.SimpleEntry<String, String>> testData = Arrays.asList(
            new AbstractMap.SimpleEntry<>("1111", "2222"),
            new AbstractMap.SimpleEntry<>("1211", "1311")
    );

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testCalculateAndPrint() throws Throwable {
        for (final AbstractMap.SimpleEntry<String, String> entry : testData) {
            runInputOutputTest(entry.getKey(), entry.getValue());
        }
    }
}
