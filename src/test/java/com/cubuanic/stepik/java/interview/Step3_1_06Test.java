package com.cubuanic.stepik.java.interview;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step3_1_06.instantToDate;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class Step3_1_06Test {
    public static class InstantToDateSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class InstantToDateParameterizedTests {
        private LocalDateTime from;
        private String to;

        public InstantToDateParameterizedTests(LocalDateTime from, String to) {
            this.from = from;
            this.to = to;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {
                            LocalDateTime.parse("2019-10-06T15:13:27.961843900Z", ISO_DATE_TIME),
                            "2019-10-06T15:13:27.961Z"
                    },
                    {LocalDateTime.MIN, "292269055-12-02T16:47:04.192Z"},
                    {LocalDateTime.MAX, "292278994-08-17T07:12:55.807Z"},
            });
        }

        @Test(timeout = 5000)
        public void testInstantToDate() {
            Instant instant = from.toInstant(ZoneOffset.UTC);

            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

            final Date date = instantToDate(instant);
            assertEquals(
                    from.toString(),
                    to,
                    simpleDateFormat.format(date)
            );
        }
    }
}