package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step4_2_06.A;
import com.cubuanic.stepik.java.interview.step4_2_06.B;
import com.cubuanic.stepik.java.interview.step4_2_06.Base;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;
import static com.cubuanic.stepik.java.interview.Step4_2_06.callMethodByWeight;

@RunWith(Enclosed.class)
public class Step4_2_06Test {
    public static class CallMethodWithLoggingSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class CallMethodWithLoggingParameterizedTests {
        private Base object;
        private String expected;

        public CallMethodWithLoggingParameterizedTests(Base object, String expected) {
            this.object = object;
            this.expected = expected;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {
                    new A(),
                    "Before call method1\n" +
                        "A method1\n" +
                        "After call method1\n" +
                        "A method2"
                },
                {
                    new B(),
                    "B method1\n" +
                        "Before call method2\n" +
                        "B method2\n" +
                        "After call method2"
                },
            });
        }

        @Test(timeout = 5000)
        public void testCallMethodWithLogging() throws Throwable {
            runOutputTest(
                ignore -> {
                    try {
                        Base proxy = callMethodByWeight(object);
                        proxy.method1();
                        proxy.method2();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                null,
                object.getClass().getSimpleName(),
                expected
            );
        }
    }
}