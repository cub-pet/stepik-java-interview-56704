package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_4_06.generateBadList;

public class Step2_4_06Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000, expected = ClassCastException.class)
    public void testGenerateBadList() {
        final List<Integer> list = generateBadList();
        int sum = 0;
        for (Integer i : list) {
            sum += i;
        }
    }
}