package com.cubuanic.stepik.java.interview;


import com.cubuanic.stepik.java.interview.step2_6_10.Node;
import com.cubuanic.stepik.java.interview.step2_6_10.NodeImpl;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_6_10.resetParents;
import static com.cubuanic.stepik.java.interview.Step2_6_10.setParents;
import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class Step2_6_10Test {
    public static class SetParentsSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class NodeToStringTests {
        public static Node f = new NodeImpl("f");
        public static Node e = new NodeImpl("e");
        public static Node d = new NodeImpl("d");
        public static Node c = new NodeImpl("c", e, f);
        public static Node b = new NodeImpl("b", d);
        public static Node a = new NodeImpl("a", b, c);
        private String message;
        private String expected;
        private Node tree;

        public NodeToStringTests(String message, String expected, Node tree) {
            this.message = message;
            this.expected = expected;
            this.tree = tree;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {"toStringF", "f(c,?,?)", f},
                {"toStringE", "e(c,?,?)", e},
                {"toStringD", "d(b,?,?)", d},
                {"toStringC", "c(a,e,f)", c},
                {"toStringB", "b(a,d,?)", b},
                {"toStringA", "a(?,b,c)", a},
            });
        }

        @Test(timeout = 5000)
        public void testToString() {
            assertEquals(message, expected, tree.toString());
        }
    }

    @RunWith(Parameterized.class)
    public static class SetParentsParameterizedTest {
        public static Node f = new NodeImpl("f");
        public static Node e = new NodeImpl("e");
        public static Node d = new NodeImpl("d");
        public static Node c = new NodeImpl("c", e, f);
        public static Node b = new NodeImpl("b", d);
        public static Node a = new NodeImpl("a", b, c);
        public static Node[] fSet = new Node[]{f};
        public static Node[] cefSet = new Node[]{c, e, f};
        public static Node[] efcSet = new Node[]{e, f, c};
        public static Node[] dbSet = new Node[]{d, b};
        public static Node[] abcdefSet = new Node[]{a, b, c, d, e, f};

        private String message;
        private String expected;
        private Node[] nodes;

        public SetParentsParameterizedTest(String message, String expected, Node[] nodes) {
            this.message = message;
            this.expected = expected;
            this.nodes = nodes;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {"parentsF", "f(?,?,?)", fSet},
                {"parentsCEF", "c(?,e,f);e(c,?,?);f(c,?,?)", cefSet},
                {"parentsEFC", "e(c,?,?);f(c,?,?);c(?,e,f)", efcSet},
                {"parentsDB", "d(b,?,?);b(?,d,?)", dbSet},
                {"parentsABCDEF", "a(?,b,c);b(a,d,?);c(a,e,f);d(b,?,?);e(c,?,?);f(c,?,?)", abcdefSet},
            });
        }

        @Test(timeout = 5000)
        public void testParents() {
            List<Node> list = Arrays.asList(nodes);
            resetParents(list);
            setParents(list);
            assertEquals(
                message,
                expected,
                Arrays.stream(list.toArray())
                    .map(s -> String.valueOf(s))
                    .collect(Collectors.joining(";"))
            );
        }
    }
}