package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import static com.cubuanic.TestUtils.assertOutputEquals;
import static com.cubuanic.TestUtils.checkClassInstantiation;

public class Step2_1_10Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testMain() throws Throwable {
        assertOutputEquals("90");
    }

}