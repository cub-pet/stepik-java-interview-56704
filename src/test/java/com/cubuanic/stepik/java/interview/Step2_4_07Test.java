package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_4_07.genConcurrentModificationException;

public class Step2_4_07Test implements Runnable {
    List<Integer> common;

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000, expected = ConcurrentModificationException.class)
    public void testGenConcurrentModificationException() {
        final List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(i);
        }
        common = list;

        new Thread(this).start();
        genConcurrentModificationException(common);
    }

    @Override
    public void run() {
        while (common.size() > 0) {
            common.remove(0);
            common.add(0);
        }
    }
}