package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step1_2_06.AppleImpl;
import com.cubuanic.stepik.java.interview.step1_2_06.BananaAppleImpl;
import com.cubuanic.stepik.java.interview.step1_2_06.BananaImpl;
import org.junit.Test;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;
import static com.cubuanic.stepik.java.interview.Step1_2_06.checkFruitCount;

public class Step1_2_06Test {
    List<AbstractMap.SimpleEntry<String[], String>> testData = Arrays.asList(
        new AbstractMap.SimpleEntry<>(new String[]{}, "banana=0, apple=0"),
        new AbstractMap.SimpleEntry<>(new String[]{"orange"}, "banana=0, apple=0"),
        new AbstractMap.SimpleEntry<>(new String[]{"apple", "orange"}, "banana=0, apple=1"),
        new AbstractMap.SimpleEntry<>(new String[]{"banana", "orange"}, "banana=1, apple=0"),
        new AbstractMap.SimpleEntry<>(new String[]{"bananaapple", "orange"}, "banana=1, apple=1"),
        new AbstractMap.SimpleEntry<>(new String[]{"banana", "orange", "banana"}, "banana=2, apple=0")
    );

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testCheckSortArrayLength() throws Throwable {
        for (final AbstractMap.SimpleEntry<String[], String> entry : testData) {
            String[] input = entry.getKey();
            Object[] inputArray = Arrays.stream(input).map(this::strToObject).toArray();
            runOutputTest(
                objects -> checkFruitCount(objects),
                inputArray,
                Arrays.toString(inputArray),
                entry.getValue());
        }
    }

    private Object strToObject(String s) {
        switch (s) {
            case "apple":
                return new AppleImpl();
            case "banana":
                return new BananaImpl();
            case "bananaapple":
                return new BananaAppleImpl();
        }
        return new Object();
    }


}