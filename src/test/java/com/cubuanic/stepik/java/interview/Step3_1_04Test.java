package com.cubuanic.stepik.java.interview;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step3_1_04.format;
import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class Step3_1_04Test {
    public static class DateTimeFormatterSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class DateTimeFormatterParameterizedTests {
        private String from;
        private String to;

        public DateTimeFormatterParameterizedTests(String from, String to) {
            this.from = from;
            this.to = to;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {"2007-05-14T21:27", "2007-M5-W3-Mon:21:27"},
                    {"2002-01-01T13:24", "2002-M1-W1-Tue:13:24"},
            });
        }

        @Test(timeout = 5000)
        public void testDateTimeFormatter() {
            DateTimeFormatter formatter = format();
            assertEquals(from,
                    to,
                    LocalDateTime
                            .parse(from, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"))
                            .format(formatter));
        }
    }
}