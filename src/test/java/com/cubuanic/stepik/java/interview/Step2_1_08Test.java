package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_1_08.reverse;
import static org.junit.Assert.assertEquals;

public class Step2_1_08Test {
    List<AbstractMap.SimpleEntry<Integer, Integer>> testData = Arrays.asList(
        new AbstractMap.SimpleEntry<>(0, 0),
        new AbstractMap.SimpleEntry<>(1, 1),
        new AbstractMap.SimpleEntry<>(100, 1),
        new AbstractMap.SimpleEntry<>(12345, 54321),
        new AbstractMap.SimpleEntry<>(123454321, 123454321),
        new AbstractMap.SimpleEntry<>(-1, -1),
        new AbstractMap.SimpleEntry<>(-100, -1),
        new AbstractMap.SimpleEntry<>(-12345, -54321),
        new AbstractMap.SimpleEntry<>(-123454321, -123454321)
    );

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testReverse() {
        for (final AbstractMap.SimpleEntry<Integer, Integer> entry : testData) {
            assertEquals((int) entry.getValue(), reverse(entry.getKey()));
        }
    }
}