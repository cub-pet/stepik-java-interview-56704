package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step4_2_05.A;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;
import static com.cubuanic.stepik.java.interview.Step4_2_05.callMethodByWeight;

@RunWith(Enclosed.class)
public class Step4_2_05Test {
    public static class CallMethodByWeightSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class CallMethodByWeightParameterizedTests {
        private Object object;
        private String expected;

        public CallMethodByWeightParameterizedTests(Object object, String expected) {
            this.object = object;
            this.expected = expected;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {new A(), "method2\nmethod1\nmethod3"},
            });
        }

        @Test //(timeout = 5000)
        public void testCallMethodByName() throws Throwable {
            runOutputTest(
                ignore -> {
                    try {
                        callMethodByWeight(object);
                    } catch (Exception ignored) {
                        // ignored
                        ignored.printStackTrace();
                    }
                },
                null,
                object.getClass().getSimpleName(),
                expected
            );
        }
    }
}