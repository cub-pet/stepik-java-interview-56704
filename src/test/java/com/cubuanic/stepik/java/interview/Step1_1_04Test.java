package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_1_04Test {
    List<String> positiveTestData = Arrays.asList(
            "123454321",
            "madam i’m Adam",
            "Olson in Oslo",
            "1",
            "2"
    );
    List<String> negativeTestData = Arrays.asList(
            "FizzBuzz",
            "Fizz",
            "Buzz"
    );

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void positiveTest() throws Throwable {
        for (String positiveTestDatum : positiveTestData) {
            runInputOutputTest(positiveTestDatum, "yes");
        }
    }

    @Test(timeout = 5000)
    public void negativeTest() throws Throwable {
        for (String negativeTestDatum : negativeTestData) {
            runInputOutputTest(negativeTestDatum, "no");
        }
    }
}
