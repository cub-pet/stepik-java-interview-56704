package com.cubuanic.stepik.java.interview.step2_6_08;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.runners.Parameterized.Parameters;

@RunWith(Enclosed.class)
public class FibonacciIteratorTest {
    static int fib(int n) {
        FibonacciIterator it = new FibonacciIterator();
        int fib = 0;
        for (int i = 0; i <= n; i++) {
            fib = it.next();
        }
        return fib;
    }

    public static class FibonacciIteratorSingleTests {

        @Test(timeout = 5000)
        public void testFib0() {
            assertEquals("fib0", 0, fib(0));
        }

        @Test(timeout = 5000)
        public void testFib1() {
            assertEquals("fib1", 1, fib(1));
        }

        @Test(timeout = 5000)
        public void testFib5() {
            assertEquals("fib5", 5, fib(5));
        }

        @Test(timeout = 5000)
        public void testFib42() {
            assertEquals("fib42", 267914296, fib(42));
        }
    }

    @RunWith(Parameterized.class)
    public static class FibonacciIteratorParameterizedTest {
        private int n;
        private int fibn;
        private boolean hasNext;

        public FibonacciIteratorParameterizedTest(int n, int fibn, boolean hasNext) {
            this.n = n;
            this.fibn = fibn;
            this.hasNext = hasNext;
        }

        @Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {0, 0, true},
                    {1, 1, true},
                    {2, 1, true},
                    {3, 2, true},
                    {4, 3, true},
                    {5, 5, true},
                    {6, 8, true},
                    {7, 13, true},
                    {8, 21, true},
                    {9, 34, true},
                    {10, 55, true},
                    {11, 89, true},
                    {12, 144, true},
                    {13, 233, true},
                    {14, 377, true},
                    {15, 610, true},
                    {16, 987, true},
                    {17, 1597, true},
                    {18, 2584, true},
                    {19, 4181, true},
                    {20, 6765, true},
                    {21, 10946, true},
                    {22, 17711, true},
                    {23, 28657, true},
                    {24, 46368, true},
                    {25, 75025, true},
                    {26, 121393, true},
                    {27, 196418, true},
                    {28, 317811, true},
                    {29, 514229, true},
                    {30, 832040, true},
                    {31, 1346269, true},
                    {32, 2178309, true},
                    {33, 3524578, true},
                    {34, 5702887, true},
                    {35, 9227465, true},
                    {36, 14930352, true},
                    {37, 24157817, true},
                    {38, 39088169, true},
                    {39, 63245986, true},
                    {40, 102334155, true},
                    {41, 165580141, true},
                    {42, 267914296, true},
                    {43, 433494437, true},
                    {44, 701408733, true},
                    {45, 1134903170, true},
                    {46, 1836311903, false}
            });
        }

        @Test(timeout = 5000)
        public void testIterator() {
            Iterator<Integer> it = new FibonacciIterable().iterator();
            int fib = 0;
            for (int i = 0; i <= n; i++) {
                fib = it.next();
            }

            assertEquals(
                    "val" + n,
                    (int) fibn,
                    (int) fib
            );
            assertEquals(
                    "next" + n,
                    hasNext,
                    it.hasNext()
            );
        }
    }
}