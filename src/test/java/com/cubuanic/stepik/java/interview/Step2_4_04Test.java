package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_4_04.generateOutOfMemoryError;

public class Step2_4_04Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000, expected = OutOfMemoryError.class)
    public void testGenerateOutOfMemoryError() {
        generateOutOfMemoryError();
    }
}