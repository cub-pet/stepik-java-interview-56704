package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step4_3_02.A;
import com.cubuanic.stepik.java.interview.step4_3_02.B;
import com.cubuanic.stepik.java.interview.step4_3_02.Operation;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;
import static com.cubuanic.stepik.java.interview.Step4_3_02.createPoolOperation;

@RunWith(Enclosed.class)
public class Step4_3_02Test {
    public static class CreatePoolOperationSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class CreatePoolOperationParameterizedTests {
        private String name;
        private List<Object> objects;
        private List<String> urls;
        private String expected;

        public CreatePoolOperationParameterizedTests(String name, List<Object> objects, List<String> urls, String expected) {
            this.name = name;
            this.objects = objects;
            this.urls = urls;
            this.expected = expected;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {
                    "case 1",
                    Arrays.asList(new A()),
                    Arrays.asList(
                        "hello",
                        "hello",
                        "goodbye"
                    ),
                    "hello world!!!\n" +
                        "hello world!!!\n" +
                        "goodbye my friend!!!"
                },
                {
                    "case 1",
                    Arrays.asList(new A(), new B()),
                    Arrays.asList(
                        "hello",
                        "palindrome",
                        "cat",
                        "dog",
                        "goodbye"
                    ),
                    "hello world!!!\n" +
                        "madam I'm adam\n" +
                        "A cat may look at a king\n" +
                        "A barking dog never bites\n" +
                        "goodbye my friend!!!"
                },
            });
        }

        @Test(timeout = 5000)
        public void testCreatePoolOperation() throws Throwable {
            runOutputTest(
                ignore -> {
                    try {
                        final Map<String, Operation> poolOperations = createPoolOperation(objects);
                        for (String url : urls) {
                            poolOperations.get(url).call();
                        }
                    } catch (Exception e) {
                        System.out.println(e.getClass().getSimpleName());
                    }
                },
                null,
                name,
                expected
            );
        }

    }
}