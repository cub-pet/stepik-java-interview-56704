package com.cubuanic.stepik.java.interview;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step3_1_05.toMoscowTime;
import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class Step3_1_05Test {
    public static class ToMoscowTimeSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class ToMoscowTimeParameterizedTests {
        private String from;
        private String tz;
        private String to;

        public ToMoscowTimeParameterizedTests(String from, String tz, String to) {
            this.from = from;
            this.tz = tz;
            this.to = to;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {"2007-05-14T21:27", "Europe/Moscow", "2007-05-14T21:27"},
                    {"2002-01-01T23:55", "Europe/Berlin", "2002-01-02T01:55"},
            });
        }

        @Test(timeout = 5000)
        public void testToMoscowTime() {
            LocalDateTime fromLDT = LocalDateTime.parse(
                    from,
                    DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")
            );
            assertEquals(
                    from + " " + tz,
                    to,
                    toMoscowTime(fromLDT, tz).toString()
            );
        }
    }
}