package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step2_3_04.Complex;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_3_04.createComplex;
import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class Step2_3_04Test {
    public static double delta = 0.01;

    public static class CreateComplexSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class CreateComplexParameterizedTests {
        private double ar;
        private double ai;
        private double br;
        private double bi;

        public CreateComplexParameterizedTests(double ar, double ai, double br, double bi) {
            this.ar = ar;
            this.ai = ai;
            this.br = br;
            this.bi = bi;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {1, 1, 7, -3.321},
                {1, 1, 1, 1},
                {6.13, 71812.1221, -9.12121212, -0.121121},
            });
        }

        @Test(timeout = 5000)
        public void testCreateComplex() {
            Complex a = createComplex(ar, ai);
            Complex b = createComplex(br, bi);

            Complex sum = a.sum(b);
            Complex sub = a.sub(b);
            Complex mul = a.mul(b);

            assertEquals("sum-r", ar + br, sum.getReal(), delta);
            assertEquals("sum-i", ai + bi, sum.getImage(), delta);

            assertEquals("sub-r", ar - br, sub.getReal(), delta);
            assertEquals("sub-i", ai - bi, sub.getImage(), delta);

            assertEquals("mul-r", ar * br - ai * bi, mul.getReal(), delta);
            assertEquals("mul-i", ar * bi + ai * br, mul.getImage(), delta);

            assertEquals("eq-i", ar == br && ai == bi, a.equals(b));

            assertEquals("str-i", round(ar) + (ai < 0 ? "-" + round(ai) + "i": ai == 0 ? "" : "+" + round(ai)+ "i"), a.toString());
        }

        private String round(double value) {
            return String.valueOf(Math.round(value * 100.0) / 100.0);
        }
    }
}