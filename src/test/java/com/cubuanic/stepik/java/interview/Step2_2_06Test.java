package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_2_06.calculate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class Step2_2_06Test {
    List<SimpleEntry<String, Integer>> testData = Arrays.asList(
        new SimpleEntry<>("2+2*(3+4)", 16),
        new SimpleEntry<>("72/12-8*(1+4)", -34),
        new SimpleEntry<>("2*(3+2*(1+2*(1+3)))", 42),
        new SimpleEntry<>("1+2*3+(4*5+6)*7", 189),
        new SimpleEntry<>("2", 2),
        new SimpleEntry<>("-2", -2),
        new SimpleEntry<>("22+2*2", 26),
        new SimpleEntry<>("-(-(2*(3+2*(1+2*(((1+3)))))))", 42)
    );
    List<SimpleEntry<String, String>> failData = Arrays.asList(
        new SimpleEntry<>("2 2", "Bad char ' '"),
        new SimpleEntry<>("22+N", "Bad char 'N'"),
        new SimpleEntry<>("22+2+", "empty T3"),
        new SimpleEntry<>("(2))", "extra ) after E"),
        new SimpleEntry<>("2(2)", "extra ( after E"),
        new SimpleEntry<>("(2)23", "extra 23 after E"),
        new SimpleEntry<>("2*", "empty T3"),
        new SimpleEntry<>("2*(2+2", "missing )"),
        new SimpleEntry<>("2*((2+2)23", "no ) after E"),
        new SimpleEntry<>("2*(2+2)+", "empty T3"),
        new SimpleEntry<>("2*(", "empty T3"),
        new SimpleEntry<>("2*()", "unexpected in T3"),
        new SimpleEntry<>("2*)", "unexpected in T3"),
        new SimpleEntry<>("*", "unexpected in T3"),
        new SimpleEntry<>("/", "unexpected in T3"),
        new SimpleEntry<>("+", "unexpected in T3"),
        new SimpleEntry<>("-", "empty T3")
    );

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testSuccess() {
        for (final SimpleEntry<String, Integer> entry : testData) {
            try {
                assertEquals(entry.getKey(), (int) entry.getValue(), calculate(entry.getKey()));
            } catch (RuntimeException e) {
                e.printStackTrace();
                fail(entry.getKey() + ": " + e.getClass().getSimpleName());
            }
        }
    }

    @Test(timeout = 5000)
    public void testFailure() {
        for (final SimpleEntry<String, String> entry : failData) {
            try {
                calculate(entry.getKey());
                fail("No failure for " + entry.getKey());
            } catch (RuntimeException e) {
                assertEquals(entry.getKey(), entry.getValue(), e.getMessage());
            }
        }
    }
}