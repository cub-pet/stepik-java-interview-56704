package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step2_5_04.Box;
import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step2_5_04.boxingValue;

public class Step2_5_04Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000, expected = ClassCastException.class)
    public void testBoxingValue() {
        Box<Integer> result = boxingValue(5.0d);
        int x = result.getValue();
    }
}