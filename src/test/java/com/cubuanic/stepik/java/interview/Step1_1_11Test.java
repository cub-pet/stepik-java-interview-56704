package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step1_1_11.Node;
import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.stepik.java.interview.Step1_1_11.treeLeafCount;
import static org.junit.Assert.assertEquals;

public class Step1_1_11Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void test1() {
        assertEquals(0, treeLeafCount(null));
    }

    @Test(timeout = 5000)
    public void test2() {
        Node a = new Node();
        assertEquals(1, treeLeafCount(a));
    }

    @Test(timeout = 5000)
    public void test3() {
        Node a = new Node();
        Node b = new Node();
        a.setLeft(b);
        assertEquals(1, treeLeafCount(a));
    }

    @Test(timeout = 5000)
    public void test4() {
        Node a = new Node();
        Node b = new Node();
        a.setRight(b);
        assertEquals(1, treeLeafCount(a));
    }

    @Test(timeout = 5000)
    public void test5() {
        Node a = new Node();
        Node b = new Node();
        Node c = new Node();
        a.setRight(b);
        b.setRight(c);
        assertEquals(1, treeLeafCount(a));
    }

    @Test(timeout = 5000)
    public void test6() {
        Node a = new Node();
        Node b = new Node();
        Node c = new Node();
        a.setLeft(b);
        b.setLeft(c);
        assertEquals(1, treeLeafCount(a));
    }

    @Test(timeout = 5000)
    public void test7() {
        Node a = new Node();
        Node b = new Node();
        Node c = new Node();
        a.setLeft(b);
        a.setRight(c);
        assertEquals(2, treeLeafCount(a));
    }

    @Test(timeout = 5000)
    public void test8() {
        Node a = new Node();
        Node b = new Node();
        Node c = new Node();
        a.setLeft(b);
        b.setRight(c);
        assertEquals(1, treeLeafCount(a));
    }

    @Test(timeout = 5000)
    public void test9() {
        Node a = new Node();
        Node b = new Node();
        Node c = new Node();
        Node d = new Node();
        a.setLeft(b);
        b.setRight(c);
        c.setRight(d);
        assertEquals(1, treeLeafCount(a));
    }
}
