package com.cubuanic.stepik.java.interview;

import com.cubuanic.stepik.java.interview.step4_1_03.A;
import com.cubuanic.stepik.java.interview.step4_1_03.B;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;
import static com.cubuanic.stepik.java.interview.Step4_1_03.printAllClassFields;

@RunWith(Enclosed.class)
public class Step4_1_03Test {
    public static class PrintAllClassFieldsSingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class PrintAllClassFieldsParameterizedTests<T> {
        private Class<T> clazz;
        private String clazzName;
        private String output;

        public PrintAllClassFieldsParameterizedTests(Class<T> clazz, String clazzName, String output) {
            this.clazz = clazz;
            this.clazzName = clazzName;
            this.output = output;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {A.class, "A", "age\nname"},
                {B.class, "B", "a\ncmp"},
            });
        }

        @Test(timeout = 5000)
        public void testPrintAllClassFields() throws Throwable {
            runOutputTest(
                clazz -> printAllClassFields(clazz),
                clazz,
                clazzName,
                output);
        }
    }
}