package com.cubuanic.stepik.java.interview;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_1_03Test {
    List<String> testData = Arrays.asList(
            "FizzBuzz",
            "1",
            "2",
            "Fizz",
            "4",
            "Buzz",
            "Fizz",
            "7",
            "8",
            "Fizz",
            "Buzz",
            "11",
            "Fizz",
            "13",
            "14",
            "FizzBuzz",
            "16",
            "17",
            "Fizz",
            "19"
    );

    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testFizzBuzz() throws Throwable {
        for (int i = 0; i < testData.size(); i++) {
            runInputOutputTest(String.valueOf(i), testData.get(i));
        }
    }
}
