This is code for online course https://stepik.org/course/56704

There is nothing specific, just solving course tasks.

However, the code is clean and covered by tests, coverage is close to 100%.

The most advanced task in this course, as for me, was expression calculator - Step2\_2\_06 (parser, tokenizer, RPN stack evaluator).
